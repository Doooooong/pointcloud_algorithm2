#include "Visualizer.h"
#include "debug.hpp"

#if defined(_WIN32) || defined(WIN32)
#include <direct.h>
#elif defined(__linux__) || defined(__linux)
#include <unistd.h>
#include <boost/make_shared.hpp>
#endif

#if defined(_WIN32) || defined(WIN32)
const char _PATH_SPLIT_ = '\\';
#elif defined(__linux__) || defined(__linux)
const char _PATH_SPLIT_ = '/';
#endif


pcl::visualization::Camera CamParatemer;

void PCL_Visualizer_KeyEvent_CamPara_Process(const pcl::visualization::KeyboardEvent& event, void* VisPtr)
{
	if (event.getKeySym() == "c" && event.keyDown())
	{
		if (event.isCtrlPressed())
		{
			pcl::visualization::PCLVisualizer* tmp2 = (static_cast< pcl::visualization::PCLVisualizer* > (VisPtr));
			pcl::visualization::PCLVisualizer::Ptr Vis_ptr = std::make_shared<pcl::visualization::PCLVisualizer >(*tmp2);
//			Vis_ptr->getCameraParameters(CamParatemer);
			std::cout << "PCL_Visualizer_KeyEvent_CamPara_Process:getCameraParameters!" << std::endl;
		}
	}
	else if (event.getKeySym() == "v" && event.keyDown())
	{
		if (event.isCtrlPressed())
		{
			pcl::visualization::PCLVisualizer* tmp2 = (static_cast< pcl::visualization::PCLVisualizer* > (VisPtr));
			pcl::visualization::PCLVisualizer::Ptr Vis_ptr = std::make_shared<pcl::visualization::PCLVisualizer >(*tmp2);
//			Vis_ptr->setCameraParameters(CamParatemer);
			std::cout << "PCL_Visualizer_KeyEvent_CamPara_Process:setCameraParameters!" << std::endl;
		}
	}
}
void PCL_Visualizer_PickEventProcess(const pcl::visualization::PointPickingEvent& event, void* PCD_voidPointer)
{
	PointXYZ tmp;
	event.getPoint(tmp.x, tmp.y, tmp.z);
	PickedPC.push_back(tmp);
	PickedPointIndices.indices.push_back(event.getPointIndex());
	std::cout << "PCL_Visualizer_PickEventProcess: Picked!" << std::endl;
}
void PCL_Visualizer_KeyEventProcess(const pcl::visualization::KeyboardEvent& event, void* PCD_voidPointer)
{
	if (event.getKeySym() == "s" && event.keyDown())
	{
		//////// Convert the (void*) to (PCXYZ_Ptr)
		PCXYZRGB* tmp = (static_cast< PCXYZRGB* > (PCD_voidPointer));
		PCXYZRGB_Ptr ptr = std::make_shared<PCXYZRGB >(*tmp);

		const int PathStrLen = 200;
		char Path[PathStrLen] = "";
#if defined(_WIN32) || defined(WIN32)
		std::string WorkPath(_getcwd(Path, PathStrLen));
#elif defined(__linux__) || defined(__linux)
		std::string WorkPath(getcwd(Path, PathStrLen));
#endif
		std::string savePath = "";
		std::string FileName = "";
		FileName = get_CurrtenTime(true) + ".pcd";
		savePath = (std::string)"." + _PATH_SPLIT_ + FileName;
		//pcl::io::savePCDFileBinary(savePath, *ptr);
		pcl::io::savePCDFileASCII(savePath, *ptr);
		std::cout << "PointCloud has been saved to " + WorkPath + _PATH_SPLIT_ + FileName << std::endl;
	}
	else if (event.getKeySym() == "i" && event.keyDown())
	{
		//////// Convert the (void*) to (PCXYZ_Ptr)
		PCXYZRGB* tmp = (static_cast< PCXYZRGB* > (PCD_voidPointer));
		PCXYZRGB_Ptr ptr = std::make_shared<PCXYZRGB >(*tmp);

		std::cout << "The size of PointCloud is: " << (*tmp).points.size() << std::endl;
	}
	else if (event.getKeySym() == "h" && event.keyDown())
	{
		PCL_WARN( "ONLY FOR PC_Show or PC_Show_Stuck : Press key 's' to save current pointcloud, Press 'i' to print number of points in point cloud.\r\n");
	}
}


void InitialVisualizer(PCLVis_Ptr &Viewer, std::string Captain, ViewerPara para)
{
//	std::cout << "Probe3.1" << std::endl;
	if (Captain != "")
		Viewer->setWindowName(Captain);
//	std::cout << "Probe3.3" << std::endl;
//	std::cout << para.BG_Color[0] << para.BG_Color[1] << para.BG_Color[2] << std::endl;
	Viewer->setBackgroundColor(para.BG_Color[0], para.BG_Color[1], para.BG_Color[2], 0);
//	std::cout << "Probe3.4" << std::endl;

	if(para.NeedBaseCoord)
		Viewer->addCoordinateSystem(para.Coordinate_Scale, "CoordOringin");
//	std::cout << "Probe3.5" << std::endl;
	if(para.NeedCoords)
		for (int i = 0; i < para.TransformMats.size(); ++i)
			Viewer->addCoordinateSystem(para.Coordinate_Scale, para.TransformMats[i], "Coord" + std::to_string(i));
//	std::cout << "Probe3.6" << std::endl;
}

void StartDisplay(pcl::visualization::PCLVisualizer::Ptr Viewer)
{
	while (!Viewer->wasStopped())
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(13));
		Viewer->spinOnce(5);
	}
	Viewer->close();
}

PointXYZRGB __inner_CreateColorSingle__(int Index, int AvaliableCount, int AvaliablePC[], ViewerPara para)
{
	PointXYZRGB ColorInfo((float)0, (float)0, (float)0);
	if(para.NeedCustomizeColor)
	{
		if(AvaliablePC[Index] < para.Colors.size())
			ColorInfo = para.Colors[AvaliablePC[Index]];
		else
		{
			ColorInfo = HSV2RGB(Index * (360 / AvaliableCount), 255, 255);
			std::string msg = "WARNING: Point clouds with index " + std::to_string(AvaliablePC[Index]) + " has not specific color! \r\nIt'll display in "
					+ "[R:" + std::to_string(ColorInfo.r)
					+ "G:" + std::to_string(ColorInfo.g)
					+ "B:" + std::to_string(ColorInfo.b) + "].";
			PCL_WARN(msg.c_str());
		}
	}
	else
		ColorInfo = HSV2RGB(Index * (360 / AvaliableCount), 255, 255);
	return ColorInfo;
}
PointXYZRGB __inner_CreateColorSingle__(ViewerPara para)
{
	if(para.NeedCustomizeColor)
	{
		if(para.Colors.size() != 0)
			return para.Colors[0];
		else
		{
			std::string msg = "WARNING: Point cloud has not specific color! \r\nIt'll display in RED color!";
			PCL_WARN(msg.c_str());
			return pcl::PointXYZRGB((float)255, (float)0, (float)0);
		}
	}
	else
		return pcl::PointXYZRGB((float)255, (float)0, (float)0);
}

int CheckDataValid_ForVisualizer(PlyMesh Data, int &NumOfData, int AvaliablePC[])
{
	int Num_AvaliablePC = 0;
	for (int i = 0; i < NumOfData; i++)
	{
		if((Data).cloud.data.size() != 0)
			AvaliablePC[Num_AvaliablePC++] = i;
	}

	if(Num_AvaliablePC == 0)
	{
		PCL_ERROR("WARNING: Mesh variable has no data. Exit!");
		return 0;
	}
	else
		return Num_AvaliablePC;
}
int CheckDataValid_ForVisualizer(std::shared_ptr<PlyMesh> Data, int &NumOfData, int AvaliablePC[])
{
	int Num_AvaliablePC = 0;
	for (int i = 0; i < NumOfData; i++)
	{
		if((*Data).cloud.data.size() != 0)
			AvaliablePC[Num_AvaliablePC++] = i;
	}

	if(Num_AvaliablePC == 0)
	{
		PCL_ERROR("WARNING: Mesh_Ptr variable has no data. Exit!\r\n");
		return 0;
	}
	else
		return Num_AvaliablePC;
}
int CheckDataValid_ForVisualizer(PlyMesh Data[], int &NumOfData, int AvaliablePC[])
{
	int Num_AvaliablePC = 0;
	for (int i = 0; i < NumOfData; i++)
	{
		if((Data[i]).cloud.data.size() != 0)
			AvaliablePC[Num_AvaliablePC++] = i;
	}

	if(Num_AvaliablePC == 0)
	{
		PCL_ERROR("WARNING: Mesh[] variable has no data. Exit!\r\n");
		return 0;
	}
	else
		return Num_AvaliablePC;
}
int CheckDataValid_ForVisualizer(std::shared_ptr<PlyMesh> Data[], int &NumOfData, int AvaliablePC[])
{
	int Num_AvaliablePC = 0;
	for (int i = 0; i < NumOfData; i++)
	{
		if((*Data[i]).cloud.data.size() != 0)
			AvaliablePC[Num_AvaliablePC++] = i;
	}

	if(Num_AvaliablePC == 0)
	{
		PCL_ERROR("WARNING: Mesh_Ptr[] variable has no data. Exit!\r\n");
		return 0;
	}
	else
		return Num_AvaliablePC;
}

PCXYZRGB copyPointCloudWithColor(PCXYZ_Ptr Source, double r, double g, double b, bool NeedOriginColor)
{
	PCXYZRGB res;
	for(PointXYZ point : Source->points)
	{
		PointXYZRGB tmp;
		tmp.x = point.x;
		tmp.y = point.y;
		tmp.z = point.z;
		tmp.r = r;
		tmp.g = g;
		tmp.b = b;
		res.points.push_back(tmp);
	}
	return res;
}
PCXYZRGB copyPointCloudWithColor(PCXYZN_Ptr Source, double r, double g, double b, bool NeedOriginColor)
{
	PCXYZRGB res;
	for(PointXYZN point : Source->points)
	{
		PointXYZRGB tmp;
		tmp.x = point.x;
		tmp.y = point.y;
		tmp.z = point.z;
		tmp.r = r;
		tmp.g = g;
		tmp.b = b;
		res.points.push_back(tmp);
	}
	return res;
}

PCXYZRGB __inner_AddDataToViewer__(PCLVis_Ptr Viewer, PCXYZ_Ptr Data, int Index, PointXYZRGB ColorInfo, ViewerPara para)
{
	typedef ColorHandler_PCXYZ CHXYZ;
	Viewer->addPointCloud(Data, CHXYZ(Data, ColorInfo.r, ColorInfo.g, ColorInfo.b), "Cloud" + std::to_string(Index));
	return copyPointCloudWithColor(Data, ColorInfo.r, ColorInfo.g, ColorInfo.b, false);
}
PCXYZRGB __inner_AddDataToViewer__(PCLVis_Ptr Viewer, PCXYZN_Ptr Data, int Index, PointXYZRGB ColorInfo, ViewerPara para)
{
	typedef ColorHandler_PCXYZN	CHXYZN;

	Viewer->addPointCloud(Data, CHXYZN(Data, ColorInfo.r, ColorInfo.g, ColorInfo.b), "Cloud" + std::to_string(Index));
	if(para.NeedDisplayNormal)
	{
	// __DEBUG_PROBE__();
	// 	pcl::PointCloud<pcl::Normal>::Ptr normal_data(new pcl::PointCloud<pcl::Normal>);
	// 	for(PointXYZN p : *Data)
	// 	{
	// 		__DEBUG_PROBE__(p.normal);
	// 		normal_data->points.push_back(pcl::Normal(p.normal_x, p.normal_y, p.normal_z));
	// 	}
		// Viewer->addPointCloudNormals<PointXYZN, pcl::Normal>(Data, normal_data, 100, 0.02f, "Normal" + std::to_string(Index));
		Viewer->addPointCloudNormals<PointXYZN>(Data, 10, 0.02f, "Normal" + std::to_string(Index));
	}
	if(para.NeedDisplayNormal)
		Viewer->addPointCloudNormals<PointXYZN, PointXYZN>(Data, Data, 100, 0.02f, "Normal" + std::to_string(Index));
	return copyPointCloudWithColor(Data, ColorInfo.r, ColorInfo.g, ColorInfo.b, false);
}
PCXYZRGB __inner_AddDataToViewer__(PCLVis_Ptr Viewer, PCXYZRGB_Ptr Data, int Index, PointXYZRGB ColorInfo, ViewerPara para)
{
	typedef ColorHandler_PCXYZRGB CHXYZRGB;
	typedef ColorRGBField_RGB CHXYZRGB_F;

	if(para.NeedRGBPC_SegmentMode)  // Using segment color.
	{
		Viewer->addPointCloud(Data, CHXYZRGB(Data, ColorInfo.r, ColorInfo.g, ColorInfo.b), "Cloud" + std::to_string(Index));
		return copyPointCloudWithColor(Data, (double)ColorInfo.r, (double)ColorInfo.g, (double)ColorInfo.b, false);
	}
	else
	{
		Viewer->addPointCloud(Data, CHXYZRGB_F(Data), "Cloud" + std::to_string(Index));
		return *Data;
	}
}
PCXYZRGB __inner_AddDataToViewer__(PCLVis_Ptr Viewer, PCXYZRGBN_Ptr Data, int Index, PointXYZRGB ColorInfo, ViewerPara para)
{
	typedef ColorHandler_PCXYZRGBN	CHXYZRGBN;
	typedef ColorRGBField_RGBN CHXYZRGBN_F;

	PCXYZRGBN::ConstPtr tmp = Data;
	if(para.NeedDisplayNormal)
		Viewer->addPointCloudNormals<PointXYZRGBN, PointXYZRGBN>(Data, Data, 100, 0.02f, "Normal");
	if(para.NeedRGBPC_SegmentMode)  // Using segment color.
	{
		Viewer->addPointCloud(Data, CHXYZRGBN(Data, ColorInfo.r, ColorInfo.g, ColorInfo.b), "Cloud" + std::to_string(Index));
		return copyPointCloudWithColor(Data, (double)ColorInfo.r, (double)ColorInfo.g, (double)ColorInfo.b, false);
	}
	else
	{
		Viewer->addPointCloud(Data, CHXYZRGBN_F(Data), "Cloud" + std::to_string(Index));
		return copyPointCloudWithColor(Data, (double)ColorInfo.r, (double)ColorInfo.g, (double)ColorInfo.b, true);
	}
}
PCXYZRGB __inner_AddDataToViewer__(PCLVis_Ptr Viewer, PlyMesh Data, int Index, PointXYZRGB ColorInfo, ViewerPara para)
{
	Viewer->addPolygonMesh(Data, "PlyMesh" + std::to_string(Index));
	return PCXYZRGB();
}

void AddDataToViewer(PCLVis_Ptr Viewer, PlyMesh Data, int &Num_AvaliablePC, int AvaliablePC[], ViewerPara para)
{
	PCXYZRGB_Ptr ForSavePC(new PCXYZRGB);

	PointXYZRGB ColorInfo();
	__inner_AddDataToViewer__(Viewer, Data, 0, PointXYZRGB(), para);

//	Viewer->registerKeyboardCallback(PCL_Visualizer_KeyEventProcess, (void*)ForSavePC.get());
//	Viewer->registerPointPickingCallback(PCL_Visualizer_PickEventProcess, nullptr);
}
void AddDataToViewer(PCLVis_Ptr Viewer, std::shared_ptr<PlyMesh> Data, int &Num_AvaliablePC, int AvaliablePC[], ViewerPara para)
{
	PCXYZRGB_Ptr ForSavePC(new PCXYZRGB);

	pcl::PointXYZRGB ColorInfo();
	__inner_AddDataToViewer__(Viewer, *Data, 0, PointXYZRGB(), para);

//	Viewer->registerKeyboardCallback(PCL_Visualizer_KeyEventProcess, (void*)ForSavePC.get());
//	Viewer->registerPointPickingCallback(PCL_Visualizer_PickEventProcess, nullptr);
}
void AddDataToViewer(PCLVis_Ptr Viewer, PlyMesh Data[], int &Num_AvaliablePC, int AvaliablePC[], ViewerPara para)
{
	PCXYZRGB_Ptr ForSavePC(new PCXYZRGB);

	for (int i = 0; i < Num_AvaliablePC; i++)
		__inner_AddDataToViewer__(Viewer, Data[AvaliablePC[i]], AvaliablePC[i], PointXYZRGB(), para);

//	Viewer->registerKeyboardCallback(PCL_Visualizer_KeyEventProcess, (void*)ForSavePC.get());
//	Viewer->registerPointPickingCallback(PCL_Visualizer_PickEventProcess, nullptr);
}
void AddDataToViewer(PCLVis_Ptr Viewer, std::shared_ptr<PlyMesh> Data[], int &Num_AvaliablePC, int AvaliablePC[], ViewerPara para)
{
	PCXYZRGB_Ptr ForSavePC(new PCXYZRGB);

	for (int i = 0; i < Num_AvaliablePC; i++)
		__inner_AddDataToViewer__(Viewer, *Data[AvaliablePC[i]], AvaliablePC[i], PointXYZRGB(), para);

//	Viewer->registerKeyboardCallback(PCL_Visualizer_KeyEventProcess, (void*)ForSavePC.get());
//	Viewer->registerPointPickingCallback(PCL_Visualizer_PickEventProcess, nullptr);
}



void AddCoordinateArrow(pcl::visualization::PCLVisualizer Viewer, const Eigen::Affine3f &Transform, float Scale, std::string Perfix, bool NeedArrow)
{
	float Thickness = 100;  // Rate of Radius and Length.(Length / Radius).
	float Rate_ArrowLength = 10;  // Rate_ArrowLength = Scale / ArrowLength.
	float ArrowThickness = 25;  // Half angle of cone.
	pcl::ModelCoefficients Coeffs_X;
	pcl::ModelCoefficients Coeffs_Y;
	pcl::ModelCoefficients Coeffs_Z;
	pcl::ModelCoefficients Coeffs_X_Arrow;
	pcl::ModelCoefficients Coeffs_Y_Arrow;
	pcl::ModelCoefficients Coeffs_Z_Arrow;
	Vec3f _Pos = Transform.matrix().block<3,1>(0,3);
	Vec3f _Arrow_X = Transform.matrix().block<3,1>(0,0) * Scale;
	Vec3f _Arrow_Y = Transform.matrix().block<3,1>(0,1) * Scale;
	Vec3f _Arrow_Z = Transform.matrix().block<3,1>(0,2) * Scale;

	Coeffs_X.values.push_back(_Pos(0));
	Coeffs_X.values.push_back(_Pos(1));
	Coeffs_X.values.push_back(_Pos(2));
	Coeffs_X.values.push_back(_Arrow_X(0));
	Coeffs_X.values.push_back(_Arrow_X(1));
	Coeffs_X.values.push_back(_Arrow_X(2));
	Coeffs_X.values.push_back(Scale / Thickness);

	Coeffs_Y.values.push_back(_Pos(0));
	Coeffs_Y.values.push_back(_Pos(1));
	Coeffs_Y.values.push_back(_Pos(2));
	Coeffs_Y.values.push_back(_Arrow_Y(0));
	Coeffs_Y.values.push_back(_Arrow_Y(1));
	Coeffs_Y.values.push_back(_Arrow_Y(2));
	Coeffs_Y.values.push_back(Scale / Thickness);

	Coeffs_Z.values.push_back(_Pos(0));
	Coeffs_Z.values.push_back(_Pos(1));
	Coeffs_Z.values.push_back(_Pos(2));
	Coeffs_Z.values.push_back(_Arrow_Z(0));
	Coeffs_Z.values.push_back(_Arrow_Z(1));
	Coeffs_Z.values.push_back(_Arrow_Z(2));
	Coeffs_Z.values.push_back(Scale / Thickness);

	Viewer.addCylinder(Coeffs_X, Perfix + "_X");
	Viewer.addCylinder(Coeffs_Y, Perfix + "_Y");
	Viewer.addCylinder(Coeffs_Z, Perfix + "_Z");

	Viewer.setShapeRenderingProperties ( pcl::visualization::PCL_VISUALIZER_COLOR, .2, 0.01, 0.01, Perfix + "_X");
	Viewer.setShapeRenderingProperties ( pcl::visualization::PCL_VISUALIZER_COLOR, 0.01, .2, 0.01, Perfix + "_Y");
	Viewer.setShapeRenderingProperties ( pcl::visualization::PCL_VISUALIZER_COLOR, 0.01, 0.01, .2, Perfix + "_Z");

	if(NeedArrow)
	{
		Coeffs_X_Arrow.values.push_back(_Pos(0) + _Arrow_X(0) * (1 + 1.0 / Rate_ArrowLength));
		Coeffs_X_Arrow.values.push_back(_Pos(1) + _Arrow_X(1) * (1 + 1.0 / Rate_ArrowLength));
		Coeffs_X_Arrow.values.push_back(_Pos(2) + _Arrow_X(2) * (1 + 1.0 / Rate_ArrowLength));
		Coeffs_X_Arrow.values.push_back(-_Arrow_X(0) / Rate_ArrowLength);
		Coeffs_X_Arrow.values.push_back(-_Arrow_X(1) / Rate_ArrowLength);
		Coeffs_X_Arrow.values.push_back(-_Arrow_X(2) / Rate_ArrowLength);
		Coeffs_X_Arrow.values.push_back(ArrowThickness);

		Coeffs_Y_Arrow.values.push_back(_Pos(0) + _Arrow_Y(0) * (1 + 1.0 / Rate_ArrowLength));
		Coeffs_Y_Arrow.values.push_back(_Pos(1) + _Arrow_Y(1) * (1 + 1.0 / Rate_ArrowLength));
		Coeffs_Y_Arrow.values.push_back(_Pos(2) + _Arrow_Y(2) * (1 + 1.0 / Rate_ArrowLength));
		Coeffs_Y_Arrow.values.push_back(-_Arrow_Y(0) / Rate_ArrowLength);
		Coeffs_Y_Arrow.values.push_back(-_Arrow_Y(1) / Rate_ArrowLength);
		Coeffs_Y_Arrow.values.push_back(-_Arrow_Y(2) / Rate_ArrowLength);
		Coeffs_Y_Arrow.values.push_back(ArrowThickness);

		Coeffs_Z_Arrow.values.push_back(_Pos(0) + _Arrow_Z(0) * (1 + 1.0 / Rate_ArrowLength));
		Coeffs_Z_Arrow.values.push_back(_Pos(1) + _Arrow_Z(1) * (1 + 1.0 / Rate_ArrowLength));
		Coeffs_Z_Arrow.values.push_back(_Pos(2) + _Arrow_Z(2) * (1 + 1.0 / Rate_ArrowLength));
		Coeffs_Z_Arrow.values.push_back(-_Arrow_Z(0) / Rate_ArrowLength);
		Coeffs_Z_Arrow.values.push_back(-_Arrow_Z(1) / Rate_ArrowLength);
		Coeffs_Z_Arrow.values.push_back(-_Arrow_Z(2) / Rate_ArrowLength);
		Coeffs_Z_Arrow.values.push_back(ArrowThickness);

		Viewer.addCone(Coeffs_X_Arrow, Perfix + "_X_Arrow");
		Viewer.addCone(Coeffs_Y_Arrow, Perfix + "_Y_Arrow");
		Viewer.addCone(Coeffs_Z_Arrow, Perfix + "_Z_Arrow");

		Viewer.setShapeRenderingProperties ( pcl::visualization::PCL_VISUALIZER_COLOR, .3, 0.01, 0.01, Perfix + "_X_Arrow");
		Viewer.setShapeRenderingProperties ( pcl::visualization::PCL_VISUALIZER_COLOR, 0.01, .3, 0.01, Perfix + "_Y_Arrow");
		Viewer.setShapeRenderingProperties ( pcl::visualization::PCL_VISUALIZER_COLOR, 0.01, 0.01, .3, Perfix + "_Z_Arrow");
	}

//	Viewer.setShapeRenderingProperties ( pcl::visualization::PCL_VISUALIZER_SHADING, pcl::visualization::PCL_VISUALIZER_SHADING_PHONG, Perfix + "_X");
//	Viewer.setShapeRenderingProperties ( pcl::visualization::PCL_VISUALIZER_SHADING, pcl::visualization::PCL_VISUALIZER_SHADING_PHONG, Perfix + "_Y");
//	Viewer.setShapeRenderingProperties ( pcl::visualization::PCL_VISUALIZER_SHADING, pcl::visualization::PCL_VISUALIZER_SHADING_PHONG, Perfix + "_Z");
//	Viewer.setShapeRenderingProperties ( pcl::visualization::PCL_VISUALIZER_OPACITY, 1, Perfix + "_X");
//	Viewer.setShapeRenderingProperties ( pcl::visualization::PCL_VISUALIZER_OPACITY, 1, Perfix + "_Y");
//	Viewer.setShapeRenderingProperties ( pcl::visualization::PCL_VISUALIZER_OPACITY, 1, Perfix + "_Z");
}


pcl::PointXYZRGB HSV2RGB(double H, double S, double V, double RangeH, double RangeS, double RangeV,
	double RangeR, double RangeG, double RangeB)
{
	pcl::PointXYZRGB Res;
	int Hi = 0.0;
	if (S == 0)
	{
		Res.r = V;
		Res.g = V;
		Res.b = V;
	}
	else
	{
		H /= 60.0;
		Hi = (int)H % 6;
	}

	double f = H - Hi;
	double a = V / RangeV * (1 - S / RangeS);
	double b = V / RangeV * (1 - S / RangeS * f);
	double c = V / RangeV * (1 - S / RangeS * (1 - f));

	switch (Hi)
	{
	case 0: Res.r = V;			Res.g = c * RangeG; Res.b = a * RangeB; break;
	case 1: Res.r = b * RangeR; Res.g = V;			Res.b = a * RangeB; break;
	case 2: Res.r = a * RangeR; Res.g = V;			Res.b = c * RangeB; break;
	case 3: Res.r = a * RangeR; Res.g = b * RangeG; Res.b = V;			break;
	case 4: Res.r = c * RangeR; Res.g = a * RangeG; Res.b = V;			break;
	case 5: Res.r = V;			Res.g = a * RangeG; Res.b = b * RangeB; break;
	}
	return Res;
}

std::string get_CurrtenTime(bool NeedMillisecond)
{
	auto t = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
	std::stringstream Time;
	Time << std::put_time(std::localtime(&t), "%Y-%m-%d_%H-%M-%S");

	if (NeedMillisecond)
	{
		struct timeb tb;
		ftime(&tb);
		Time << "_" << tb.millitm;
	}
	return Time.str();
}
