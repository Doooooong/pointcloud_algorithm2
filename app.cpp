#include "app.hpp"


namespace CookTeam
{
	int SlicePointCloud(PCXYZ_Ptr Data, PCXYZ_Ptr SlicedPC[], PC_Slice res[], float Thickness, int MinPoints)
	{
		PointXYZ min, max;
		Cal_AABB(Data, min, max);

		float StartCut = min.z;
		PCXYZ_Ptr NotEnough (new PCXYZ);
		PCXYZ_Ptr PassOut (new PCXYZ);
		pcl::PassThrough<PointXYZ> PassFilter;
		PassFilter.setFilterFieldName("z");
		int i = 0;

		for(; StartCut < max.z; )
		{
			PassFilter.setFilterLimits(StartCut, StartCut + Thickness);
			PassFilter.setInputCloud(Data);

			PassFilter.filter(*PassOut);

			StartCut += Thickness;
			res[i].Thickness += Thickness;  // Accumelate thickness when number of points of current slice is not enough.
			*NotEnough += *PassOut;

			if(NotEnough->size() < MinPoints)
				continue;

			res[i].StartHeight = StartCut - res[i].Thickness;

			res[i].Sliced_PC = Compress_PC_MP(NotEnough, "xy", res[i].StartHeight);

			SlicedPC[i] = NotEnough;

			NotEnough = PCXYZ_Ptr(new PCXYZ);
			i++;
		}
		if(NotEnough->size() > 0)
		{
			res[i].StartHeight = StartCut - res[i].Thickness;
			res[i].Sliced_PC = Compress_PC_MP(NotEnough, "xy", res[i].StartHeight);
			SlicedPC[i] = NotEnough;
			return i + 1;
		}
		return i;
	}
	int SlicePointCloud2(PCXYZ_Ptr Data, PCXYZ_Ptr SlicedPC[], PC_Slice res[], float Thickness, int MinPoints, float CutoffHeight)
	{
		PointXYZ min, max;
		Cal_AABB(Data, min, max);
		if (CutoffHeight != INFINITY && (CutoffHeight - min.z > -1e-4 && CutoffHeight - max.z < 1e-4))
			max.z = CutoffHeight;

		float StartCut = min.z;
		PCXYZ_Ptr SliceTmp (new PCXYZ(*Data));

		double minz_d = min.z, maxz_d = max.z, Thickness_d = Thickness;  // For calculate length of result(use double we can get more precise).
		uint_least32_t Num = static_cast<uint_least32_t>(ceil((maxz_d - minz_d) / Thickness_d));
		if (Num == 0)
			return 0;

		std::vector<PCXYZ_Ptr> SlicedPC_tmp(Num);
		for (int i = 0; i < Num; i++)
			SlicedPC_tmp[i] = PCXYZ_Ptr(new PCXYZ);
		std::vector<PC_Slice> res_tmp(Num);

		// First slice.
		std::vector<float> LayerHeightArray(Num, -INFINITY);
		for(PointXYZ point : SliceTmp->points)
		{
	//		float slice_axis = point.z;
			if (point.z >= max.z)
				continue;
			uint_least32_t layer_idx = ceil((point.z - StartCut) / Thickness);  // Get the smallest integer that larger than given float number.
			if (layer_idx > 0)
				layer_idx--;
			if(LayerHeightArray[layer_idx] < -10)
				LayerHeightArray[layer_idx] = layer_idx * Thickness + StartCut;

			res_tmp[layer_idx].Sliced_PC->points.push_back(point);
			res_tmp[layer_idx].StartHeight = LayerHeightArray[layer_idx];
			point.z = LayerHeightArray[layer_idx];
			//SlicedPC_tmp[layer_idx] = SlicedPC[layer_idx];
			SlicedPC_tmp[layer_idx]->points.push_back(point);
		}

		for (int i = 0; i < Num; ++i)
		{
			if (LayerHeightArray[i] == -INFINITY)
			{
				LayerHeightArray.erase(LayerHeightArray.begin() + i);
				SlicedPC_tmp.erase(SlicedPC_tmp.begin() + i);
				res_tmp.erase(res_tmp.begin() + i);
				Num--;
			}
		}

		int Valid = 0;
		PCXYZ_Ptr nullPC(new PCXYZ);
		for (int i = 0; i < Num - 1; ++i)  // Last layer is no need to be processed.
		{
			if(SlicedPC_tmp[i]->points.size() < MinPoints)
			{
				for(int j=0; j < SlicedPC_tmp[i]->points.size(); j++)
				{
					SlicedPC_tmp[i]->points[j].z = SlicedPC_tmp[i + 1]->points[0].z;
					SlicedPC_tmp[i + 1]->points.push_back(SlicedPC_tmp[i]->points[j]);

					res_tmp[i + 1].Sliced_PC->push_back(res_tmp[i].Sliced_PC->points[j]);
				}
				res_tmp[i + 1].StartHeight = res_tmp[i].StartHeight;
				res_tmp[i + 1].Thickness += Thickness;
				SlicedPC_tmp[i] = nullPC;
				res_tmp[i].Sliced_PC = nullPC;
			}
			else
			{
				//res_tmp[i].StartHeight += i * Thickness + StartCut;
				res_tmp[i].Thickness += Thickness;
				Valid++;
				continue;
			}
		}
		res_tmp[Num - 1].Thickness = Thickness;
		Valid++;
		int tmpValid = 0;
		int i = 0;
		for (; i < Valid; tmpValid++)  // Last layer is no need to be processed.
		{
			if(SlicedPC_tmp[tmpValid] != nullPC)
			{
				SlicedPC[i] = SlicedPC_tmp[tmpValid];
				res[i] = res_tmp[tmpValid];
				++i;
			}
		}

		return Valid;
	}


	double VolumeCalculation_byZAxisSlice(PCXYZ_Ptr Model, Vec6f AABB, float LayerHeight)
	{
		if (AABB == Vec6f::Zero())
			AABB = Cal_AABB(Model);

		int SegmentCount = (AABB(5) - AABB(2)) / LayerHeight + 10;
		PCXYZ_Ptr* Sliced_Array = new PCXYZ_Ptr[SegmentCount];
		PC_Slice* res = new PC_Slice[SegmentCount];
		float* Area = new float[SegmentCount];
		for (int i = 0; i < SegmentCount; ++i)
		{
			Sliced_Array[i] = PCXYZ_Ptr(new PCXYZ);
			res[i].Sliced_PC = PCXYZ_Ptr(new PCXYZ);
			Area[i] = -1.0f;
		}

		int count = SlicePointCloud2(Model, Sliced_Array, res, LayerHeight, 25, AABB(5));  //
		if (count == 0)
			return 0;
	//	PCShow(Sliced_Array, "Sliced", count);
		pcl::ConvexHull<PointXYZ> chull;
		chull.setComputeAreaVolume(true);
		PCXYZ_Ptr cloud_hull(new PCXYZ);
		PCXYZ_Ptr FoundPoint_Org(new PCXYZ);
		double Volume = 0.0;
		for (int i = 0; i < count; i++)
		{
			chull.setInputCloud(Sliced_Array[i]);
			chull.reconstruct(*cloud_hull);
			Area[i] = chull.getTotalArea();
	//		Std_Out("==Slice area: ", Area[i]*1e6);
			if (i > 0)
			{
				Volume += (Area[i] + Area[i - 1]) / 2.0 * res[i].Thickness;
	//			Volume += (Area[i] ) * res[i].Thickness;
			}
	//		Std_Out("==Volume: ", Volume*1e6);
		}
		if (count == 1)
			Volume = Area[0] * res[0].Thickness;
	//	Std_Out("==Volume: ", Volume*1e6);
		return Volume;

	}
}