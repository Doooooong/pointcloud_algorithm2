#include "segmentation.hpp"

namespace CookTeam
{
/**
	 * @brief Clustering using euclidean distance.
	 * 
	 * @param Source Oringin point cloud.
	 * @param Clusters The array of result of clustering.
	 * @param Setting Settings of euclidean cluster.
	 * @param ClusterWithoutZAxis Project points to XoY plane when clustering.
	 * @note We can not get complete point cloud of object on the table. If we just use 3D clustering, it may be separate the object to many pieces. To avoid this issue, you need to use [ClusterWithoutZAxis] with [true].
	 * @return int Number of clusters.
	 */
int ExtractEuclideanCluster(PCXYZ_Ptr Source, PCXYZ_Ptr Clusters[], EC_Setting Setting, bool ClusterWithoutZAxis)
{
	PCXYZ_Ptr NoZ = PCXYZ_Ptr(new PCXYZ), calculation_input = Source;
	if (ClusterWithoutZAxis)
	{
		NoZ = Compress_PC_MP(Source, "xy");
		calculation_input = NoZ;
	}

	// Creating the KdTree object for the search method of the extraction
	pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>);
	tree->setInputCloud(calculation_input);

	std::vector<PCIDX> cluster_indices;
	pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
	ec.setClusterTolerance(Setting.ClusterTolerance); // 2cm
	ec.setMinClusterSize(Setting.MinClusterSize);
	ec.setMaxClusterSize(Setting.MaxClusterSize);
	ec.setSearchMethod(tree);
	ec.setInputCloud(calculation_input);
	ec.extract(cluster_indices);

	int i = 0;
	for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin(); it != cluster_indices.end(); ++it)
	{
		PCXYZ_Ptr cloud_cluster(new PCXYZ);
		for (std::vector<int>::const_iterator pit = it->indices.begin(); pit != it->indices.end(); ++pit)
			cloud_cluster->points.push_back(Source->points[*pit]); //*
		cloud_cluster->width = cloud_cluster->points.size();
		cloud_cluster->height = 1;
		cloud_cluster->is_dense = true;
		Clusters[i++] = cloud_cluster;
	}
	return cluster_indices.size();
}

/**
	 * @brief Clustering using euclidean distance.
	 * 
	 * @deprecated
	 * @param Source Oringin point cloud.
	 * @param Clusters The point cloud pointer array of result of clustering.
	 * @param Setting Settings of euclidean cluster.
	 * @param ClusterWithoutZAxis Project points to XoY plane when clustering.
	 * @note We can not get complete point cloud of object on the table. If we just use 3D clustering, it may be separate the object to many pieces. To avoid this issue, you need to use [ClusterWithoutZAxis] with [true].
	 * @return int Number of clusters.
	 */
int ExtractEuclideanCluster(PCXYZ_Ptr Source, PCXYZ_Ptr Clusters[])
{
	// Creating the KdTree object for the search method of the extraction
	pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>);
	tree->setInputCloud(Source);

	std::vector<pcl::PointIndices> cluster_indices;
	pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
	ec.setClusterTolerance(0.01); // 2cm
	ec.setMinClusterSize(150);
	ec.setMaxClusterSize(45000);
	ec.setSearchMethod(tree);
	ec.setInputCloud(Source);
	ec.extract(cluster_indices);

	int i = 0;
	for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin(); it != cluster_indices.end(); ++it)
	{
		PCXYZ_Ptr cloud_cluster(new PCXYZ);
		for (std::vector<int>::const_iterator pit = it->indices.begin(); pit != it->indices.end(); ++pit)
			cloud_cluster->points.push_back(Source->points[*pit]); //*
		cloud_cluster->width = cloud_cluster->points.size();
		cloud_cluster->height = 1;
		cloud_cluster->is_dense = true;
		Clusters[i++] = cloud_cluster;
	}
	return cluster_indices.size();
}

/**
	 * @brief Clustering using Region Grow method.
	 * Get more details in <a href="http://pointclouds.org/documentation/tutorials/region_growing_segmentation.php#region-growing-segmentation">Region-Growing-Segmentation</a>.
	 * @param Source Oringin point cloud.
	 * @param Clusters The point cloud pointer array of result of clustering.
	 * @param Setting Settings of region grow cluster. {@link RGC_Setting}
	 * @return int Number of clusters.
	 */
int ExtractRegionGrowCLuster(PCXYZ_Ptr Source, PCXYZ_Ptr Clusters[], RGC_Setting Setting)
{
	pcl::search::Search<PointXYZ>::Ptr tree = std::shared_ptr<pcl::search::Search<pcl::PointXYZ>>(new pcl::search::KdTree<pcl::PointXYZ>);
	pcl::PointCloud<pcl::Normal>::Ptr normals(new pcl::PointCloud<pcl::Normal>);
	pcl::NormalEstimationOMP<PointXYZ, pcl::Normal> normal_estimator;
	normal_estimator.setSearchMethod(tree);
	normal_estimator.setInputCloud(Source);
	normal_estimator.setKSearch(Setting.NormalKSearch);
	normal_estimator.compute(*normals);

	pcl::RegionGrowing<pcl::PointXYZ, pcl::Normal> reg;
	reg.setMinClusterSize(Setting.MinClusterSize);
	reg.setMaxClusterSize(Setting.MaxClusterSize);
	reg.setSearchMethod(tree);
	reg.setNumberOfNeighbours(Setting.NumberOfNeighbours);
	reg.setInputCloud(Source);
	reg.setInputNormals(normals);
	reg.setSmoothnessThreshold(Setting.SmoothnessThreshold);
	reg.setCurvatureThreshold(Setting.CurvatureThreshold);

	std::vector<pcl::PointIndices> cluster_indices;
	reg.extract(cluster_indices);

	int i = 0;
	for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin(); it != cluster_indices.end(); ++it)
	{
		pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_cluster(new pcl::PointCloud<pcl::PointXYZ>);
		for (std::vector<int>::const_iterator pit = it->indices.begin(); pit != it->indices.end(); ++pit)
			cloud_cluster->points.push_back(Source->points[*pit]); //*
		cloud_cluster->width = cloud_cluster->points.size();
		cloud_cluster->height = 1;
		cloud_cluster->is_dense = true;
		Clusters[i++] = cloud_cluster;
	}

	return cluster_indices.size();
}

#ifdef __TESTING_CODE__
int Exract_EuclideanCluster_cuda(PCXYZ_Ptr Source, PCXYZ_Ptr Clusters[])
{

	pcl::gpu::Octree::PointCloud cloud_device;
	cloud_device.upload(Source->points);

	pcl::gpu::Octree::Ptr octree_device(new pcl::gpu::Octree);
	octree_device->setCloud(cloud_device);
	octree_device->build();

	std::vector<PCIDX> cluster_indices_gpu;
	pcl::gpu::EuclideanClusterExtraction gec;
	gec.setClusterTolerance(0.02); // 2cm
	gec.setMinClusterSize(100);
	gec.setMaxClusterSize(25000);
	gec.setSearchMethod(octree_device);
	gec.setHostCloud(Source);
	gec.extract(cluster_indices_gpu);

	__DEBUG_PROBE__(cluster_indices_gpu.size()); // , __FILE__, __FUNCTION__, __LINE__

	if (cluster_indices_gpu.size() > 0)
	{
		int i = 0;

		for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices_gpu.begin(); it != cluster_indices_gpu.end(); ++it)
		{
			PCXYZ_Ptr cloud_cluster(new PCXYZ);
			cloud_cluster->resize(it->indices.size());
			int j = 0;
			for (int idx : it->indices)
				cloud_cluster->points[j++] = Source->points[idx]; //*
			cloud_cluster->width = cloud_cluster->points.size();
			cloud_cluster->height = 1;
			cloud_cluster->is_dense = true;
			Clusters[i++] = cloud_cluster;
		}
	}
	return cluster_indices_gpu.size();
}
#endif
} // namespace CookTeam