#ifndef __COOKTEAM_DEBUG_H__
#define __COOKTEAM_DEBUG_H__

#include "ConsoleColor.h"
#include <iostream>
#include <string>
#include <thread>
#include <chrono>
#include <ctime>


// #if _GLIBCXX_CHRONO
#define START_TIMER(NAME) auto NAME = std::chrono::high_resolution_clock::now()
#define STOP_TIMER(NAME) std::cout << "Timer [" << #NAME << "] use " << std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - NAME).count() << " microseconds." << std::endl
#define __DEBUG_SLEEP_MS__(DURATION) std::this_thread::sleep_for(std::chrono::milliseconds(DURATION))
// #endif


#ifndef __DISABLE_DEBUG_PROBES__
#define __DEBUG_PROBE__(...)            \
    __inner_DEBUG_PROBE__(__VA_ARGS__); \
    __DEBUG_PROBE_print_file__(__FILE__, __FUNCTION__, __LINE__)
#define __DEBUG_PROBE_WRN__(...)            \
    __inner_DEBUG_PROBE_WRN__(__VA_ARGS__); \
    __DEBUG_PROBE_WRN_print_file__(__FILE__, __FUNCTION__, __LINE__)
#define __DEBUG_PROBE_ERR__(...)            \
    __inner_DEBUG_PROBE_ERR__(__VA_ARGS__); \
    __DEBUG_PROBE_ERR_print_file__(__FILE__, __FUNCTION__, __LINE__)
#else
#define __DEBUG_PROBE__(...)
#define __DEBUG_PROBE_WRN__(...)
#define __DEBUG_PROBE_ERR__(...)  
#endif

template <typename... T>
inline void print(T... arg)
{
    (std::cout << ... << arg) << std::endl;
}
template <typename... T>
inline void print_WRN(T... arg)
{
    (std::cout << __COLOR_BOLDYELLOW__ << ... << arg) << __COLOREND__ << std::endl;
}
template <typename... T>
inline void print_ERR(T... arg)
{
    (std::cout << __COLOR_BOLDRED__ << ... << arg) << __COLOREND__ << std::endl;
}

template <typename... T>
inline void __inner_DEBUG_PROBE__(T... arg)
{
    auto _time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    (std::cout << "Debug probe " << ... << arg) << " ;Time:" << ctime(&(_time));
}
template <typename... T>
inline void __inner_DEBUG_PROBE_WRN__(T... arg)
{
    auto _time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    (std::cout << __COLOR_BOLDYELLOW__ << "Debug probe " << ... << arg) << " ;Time:" << ctime(&_time);
}
template <typename... T>
inline void __inner_DEBUG_PROBE_ERR__(T... arg)
{
    auto _time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    (std::cout << __COLOR_BOLDRED__ << "Debug probe " << ... << arg) << " ;Time:" << ctime(&_time);
}

inline void __DEBUG_PROBE_print_file__(const char *file, const char *function, int line)
{
    std::cout << " matched at: File:" << file << " ; Function: " << function << " ; Line: " << line << std::endl;
}
inline void __DEBUG_PROBE_WRN_print_file__(const char *file, const char *function, int line)
{
    std::cout << " matched at: File:" << file << " ; Function: " << function << " ; Line: " << line << __COLOREND__ << std::endl;
}
inline void __DEBUG_PROBE_ERR_print_file__(const char *file, const char *function, int line)
{
    std::cout << " matched at: File:" << file << " ; Function: " << function << " ; Line: " << line << __COLOREND__ << std::endl;
}

#endif