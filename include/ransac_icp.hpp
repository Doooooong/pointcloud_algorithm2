#ifndef __COOKTEAM_RANSAC_ICP_H__
#define __COOKTEAM_RANSAC_ICP_H__

#include "types.hpp"
#include "base_functions.hpp"
#include "calculation.hpp"

#include <pcl/ModelCoefficients.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>

#include <pcl/registration/icp.h>


namespace CookTeam
{
	//////// RANSAC
	Vector6f ExtractPlane(PCXYZ_Ptr Source, PCXYZ_Ptr Plane = PCXYZ_Ptr(new PCXYZ), PCXYZ_Ptr Rest = PCXYZ_Ptr(new PCXYZ), float ThreadHold = 0.004, int MinPoints = 0);
	Vector6f ExtractPlane(PCXYZ_Ptr Source, PCXYZ_Ptr Plane = PCXYZ_Ptr(new PCXYZ), PCXYZ_Ptr Rest = PCXYZ_Ptr(new PCXYZ), float ThreadHold = 0.004, double Percent = 0.1);
	Vector6f ExtractParallelPlane(PCXYZ_Ptr Source, Vec3f Axis, PCXYZ_Ptr Plane = PCXYZ_Ptr(new PCXYZ), PCXYZ_Ptr Rest = PCXYZ_Ptr(new PCXYZ), float ThreadHold = 0.004, int MinPoints = 0);
	Vector7f ExtractCylinder(PCXYZ_Ptr Source, PCXYZ_Ptr Plane = PCXYZ_Ptr(new PCXYZ), PCXYZ_Ptr Rest = PCXYZ_Ptr(new PCXYZ), float ThreadHold = 0.004, double Percent = 0.1);
	Vector7f ExtractCircle3D(PCXYZ_Ptr Source, PCXYZ_Ptr Circle, PCXYZ_Ptr Rest, float ThreadHold = 0.004f, double Percent = 0.1);


	//////// ICP
	typedef struct ICP_SettingStruct
	{
		double MaxCorrespondenceDistance = 1;
		double TransformationEpsilon = 1e-10;
		double EuclideanFitnessEpsilon = 10e-6;
		int MaximumIterations = 100;
		double ScoreLimit = 10e-6;

		double AngleLimit = 5;
		double TranLimit = 0.05;
	}ICP_Setting;

	Mat4f ICP_Single_Limit(PCXYZ_Ptr Source, PCXYZ_Ptr Target, PCXYZ_Ptr &Output, ICP_Setting Setting);
}

#endif