﻿// ConsoleColor.h
#ifndef _CONSOLE_COLOR_H_
#define _CONSOLE_COLOR_H_

#include <iostream>
#include <string>
#if _WIN32 || WIN32
#include <windows.h>
inline std::ostream& ConsoleColor_Blue(std::ostream &s)
{
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdout, FOREGROUND_BLUE   |FOREGROUND_GREEN|FOREGROUND_INTENSITY);
	return s;
}

inline std::ostream& ConsoleColor_Red(std::ostream &s)
{
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdout,   FOREGROUND_RED|FOREGROUND_INTENSITY);
	return s;
}

inline std::ostream& ConsoleColor_Green(std::ostream &s)
{
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdout,   FOREGROUND_GREEN|FOREGROUND_INTENSITY);
	return s;
}

inline std::ostream& ConsoleColor_Yellow(std::ostream &s)
{
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdout,   FOREGROUND_GREEN|FOREGROUND_RED|FOREGROUND_INTENSITY);
	return s;
}

inline std::ostream& ConsoleColor_White(std::ostream &s)
{
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdout,    FOREGROUND_RED|FOREGROUND_GREEN|FOREGROUND_BLUE);
	return s;
}

inline std::ostream& ConsoleColor_Black(std::ostream &s)
{
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdout,    0);
	return s;
}

inline std::ostream& ConsoleColor_Magenta(std::ostream &s)
{
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdout,   FOREGROUND_BLUE|FOREGROUND_RED|FOREGROUND_INTENSITY);
	return s;
}

inline std::ostream& ConsoleColor_Cyan(std::ostream &s)
{
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdout,    FOREGROUND_GREEN|FOREGROUND_BLUE|FOREGROUND_INTENSITY);
	return s;
}



struct color
{
	color(WORD attribute):m_color(attribute){};
	WORD m_color;
};

template <class _Elem, class _Traits>
std::basic_ostream<_Elem,_Traits>&  operator<<(std::basic_ostream<_Elem,_Traits>& i, const color& c) {
	HANDLE hStdout=GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdout,c.m_color);
	return i;
}
#endif

#if defined(_NO_NEED_CONSOLE_COLOR_)

#define __COLOR_RESET__			(std::string)""
#define __COLOR_BLACK__			(std::string)""				/* Black */
#define __COLOR_RED__			(std::string)""				/* Red */
#define __COLOR_GREEN__			(std::string)""				/* Green */
#define __COLOR_YELLOW__		(std::string)""				/* Yellow */
#define __COLOR_BLUE__			(std::string)""				/* Blue */
#define __COLOR_MAGENTA__		(std::string)""				/* Magenta */
#define __COLOR_CYAN__			(std::string)""				/* Cyan */
#define __COLOR_WHITE__			(std::string)""				/* White */

#define __COLOR_BOLDBLACK__		(std::string)""				/* Bold Black */
#define __COLOR_BOLDRED__		(std::string)""				/* Bold Red */
#define __COLOR_BOLDGREEN__		(std::string)""				/* Bold Green */
#define __COLOR_BOLDYELLOW__	(std::string)""				/* Bold Yellow */
#define __COLOR_BOLDBLUE__		(std::string)""				/* Bold Blue */
#define __COLOR_BOLDMAGENTA__	(std::string)""				/* Bold Magenta */
#define __COLOR_BOLDCYAN__		(std::string)""				/* Bold Cyan   */
#define __COLOR_BOLDWHITE__		(std::string)""				/* Bold White */
#define __COLOREND__		(std::string)""

#elif defined(__linux__)

#define __COLOR_RESET__			(std::string)"\033[0m"
#define __COLOR_BLACK__			(std::string)"\033[30m"				/* Black */
#define __COLOR_RED__			(std::string)"\033[31m"				/* Red */
#define __COLOR_GREEN__			(std::string)"\033[32m"				/* Green */
#define __COLOR_YELLOW__		(std::string)"\033[33m"				/* Yellow */
#define __COLOR_BLUE__			(std::string)"\033[34m"				/* Blue */
#define __COLOR_MAGENTA__		(std::string)"\033[35m"				/* Magenta */
#define __COLOR_CYAN__			(std::string)"\033[36m"				/* Cyan */
#define __COLOR_WHITE__			(std::string)"\033[37m"				/* White */

#define __COLOR_BOLDBLACK__		(std::string)"\033[1m\033[30m"		/* Bold Black */
#define __COLOR_BOLDRED__		(std::string)"\033[1m\033[31m"		/* Bold Red */
#define __COLOR_BOLDGREEN__		(std::string)"\033[1m\033[32m"		/* Bold Green */
#define __COLOR_BOLDYELLOW__	(std::string)"\033[1m\033[33m"		/* Bold Yellow */
#define __COLOR_BOLDBLUE__		(std::string)"\033[1m\033[34m"		/* Bold Blue */
#define __COLOR_BOLDMAGENTA__	(std::string)"\033[1m\033[35m"		/* Bold Magenta */
#define __COLOR_BOLDCYAN__		(std::string)"\033[1m\033[36m"		/* Bold Cyan   */
#define __COLOR_BOLDWHITE__		(std::string)"\033[1m\033[37m"		/* Bold White */
#define __COLOREND__			__COLOR_RESET__

#elif defined(_WIN32) ||defined(WIN32)

#define __COLOR_RESET__			ConsoleColor_White
#define __COLOR_BLACK__			ConsoleColor_Black		/* Black */
#define __COLOR_RED__			ConsoleColor_Red		/* Red */
#define __COLOR_GREEN__			ConsoleColor_Green		/* Green */
#define __COLOR_YELLOW__		ConsoleColor_Yellow		/* Yellow */
#define __COLOR_BLUE__			ConsoleColor_Blue		/* Blue */
#define __COLOR_MAGENTA__		ConsoleColor_Magenta	/* Magenta */
#define __COLOR_CYAN__			ConsoleColor_Cyan		/* Cyan */
#define __COLOR_WHITE__			ConsoleColor_White		/* White */

#define __COLOR_BOLDBLACK__		ConsoleColor_Black		/* Bold Black */
#define __COLOR_BOLDRED__		ConsoleColor_Red		/* Bold Red */
#define __COLOR_BOLDGREEN__		ConsoleColor_Green		/* Bold Green */
#define __COLOR_BOLDYELLOW__	ConsoleColor_Yellow		/* Bold Yellow */
#define __COLOR_BOLDBLUE__		ConsoleColor_Blue		/* Bold Blue */
#define __COLOR_BOLDMAGENTA__	ConsoleColor_Magenta	/* Bold Magenta */
#define __COLOR_BOLDCYAN__		ConsoleColor_Cyan		/* Bold Cyan */
#define __COLOR_BOLDWHITE__		ConsoleColor_White		/* Bold White */
#define __COLOREND__			(std::string)""						/* Windows don't need end string. So use empty string. */

#endif

#endif
