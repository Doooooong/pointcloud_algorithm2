#ifndef __COOKTEAM_SURFACE_MESH_H__
#define __COOKTEAM_SURFACE_MESH_H__

#include "types.hpp"
#include "base_include.hpp"
#include "type_convention.hpp"

//For surface reconstruction
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/surface/mls.h>

// For triangulation and mesh construct.
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/features/normal_3d.h>
#include <pcl/surface/gp3.h>

// For mash convention.
#include <pcl/geometry/triangle_mesh.h>
#include <pcl/geometry/quad_mesh.h>
#include <pcl/geometry/polygon_mesh.h>
#include <pcl/geometry/mesh_conversion.h>


namespace CookTeam
{
	class MyVertexData
	{
	public:
		typedef PointXYZ VertexData;
		MyVertexData (VertexData Data, const int id = -1) : id_ (id), _Data (Data) {}

		int  id () const {return (id_);}
		int& id ()       {return (id_);}

		VertexData  getData () const {return (_Data);}
		VertexData& getData ()       {return (_Data);}

	private:
		VertexData _Data;
		int id_;
	};
	typedef pcl::geometry::NoData NoData;
	typedef pcl::geometry::DefaultMeshTraits <int   , NoData , NoData, NoData> TraitsV;
	typedef pcl::geometry::DefaultMeshTraits <NoData, int    , NoData, NoData> TraitsHE;
	typedef pcl::geometry::DefaultMeshTraits <NoData, NoData , int   , NoData> TraitsE;
	typedef pcl::geometry::DefaultMeshTraits <NoData, NoData , NoData, int   > TraitsF;
	typedef pcl::geometry::DefaultMeshTraits <int   , int    , int   , int   > TraitsAD;
	typedef pcl::geometry::DefaultMeshTraits<pcl::PointXYZ, PointXYZ , PointXYZ, PointXYZ> MyTraits;
	//MyVertexData::VertexData b;
	typedef pcl::geometry::PolygonMesh <TraitsV>  MeshV;
	typedef pcl::geometry::PolygonMesh <TraitsHE> MeshHE;
	typedef pcl::geometry::PolygonMesh <TraitsE>  MeshE;
	typedef pcl::geometry::PolygonMesh <TraitsF>  MeshF;
	typedef pcl::geometry::PolygonMesh <TraitsAD> MeshAD;
	typedef pcl::geometry::PolygonMesh <MyTraits> MyMeshType;
	//MyMeshType::VertexDataCloud b;

	PlyMesh_Ptr CreatePlyMesh(PCXYZ_Ptr Data);
}

#endif