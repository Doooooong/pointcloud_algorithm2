#ifndef __COOKTEAM_CONVENTION_H__
#define __COOKTEAM_CONVENTION_H__

#include "types.hpp"
#include "debug.hpp"
#include "base_functions.hpp"

namespace CookTeam
{
Vec6f Cvt_TransformMatrix2xyzabc(Mat4f Data, int RotationOrd1 = 0, int RotationOrd2 = 1, int RotationOrd3 = 2);
PCXYZ_Ptr Cvt_PCXYZN_To_PCXYZ(PCXYZN_Ptr Source);

/**
     * @brief Convent 2-D array to Mat4f.
     * 
     * @tparam T Only support number type!!
     * @param Data 4x4 numberic array.
     * @return Mat4f 
     */
template <typename T>
Mat4f MakeTransformMatrix(T Data[4][4])
{
    if constexpr (!is_number_type<T>)
    {
        std::cout << "Not support!!" << std::endl;
        return Mat4f();
    }
    Mat4f tmp;
    for (int i = 0; i < 4; i++)
        for (int j = 0; j < 4; j++)
            tmp(i, j) = (float)Data[i][j];
    return tmp;
}

inline Vec3f Cvt_PointXYZ2Vec3f(PointXYZ l)
{
    Vec3f res(l.x, l.y, l.z);
    return res;
}
inline PointXYZ Cvt_Vec3f2PointXYZ(Vec3f l)
{
    PointXYZ res(l(0), l(1), l(2));
    return res;
}

// TODO: Complete all point types convert.
// TODO: Test this function.
template <typename T>
inline PCXYZ_Ptr Cvt_PointCloud2XYZ_MP(T source)
{
    if constexpr (!is_PC_type<T> || !is_PCptr_type<T>)
    {
        print(__FUNCTION__, " only support point cloud type!");
        return PCXYZ_Ptr();
    }
    
    PCXYZ_Ptr source_ptr(source);
    PCXYZ_Ptr ret(new PCXYZ);
    ret->points.reserve(source_ptr->size());
    ret->points.resize(source_ptr->size());

    tbb::parallel_for(tbb::blocked_range<size_t>(0, source_ptr->size()), [&](const tbb::blocked_range<size_t> &r) {
        for (size_t i = r.begin(); i != r.end(); ++i)
        {
            ret->points[i].x = source_ptr->points[i].x;
            ret->points[i].y = source_ptr->points[i].y;
            if constexpr (std::is_same<T, PCXY>::value || std::is_same<T, PCXY_Ptr>::value)
            {
                ret->points[i].z = source_ptr->points[i].z;
            }
        }
    });
    return ret;
}
} // namespace CookTeam
#endif