/**
 * @file Functions.h
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2019-06-05
 * 
 * @copyright Copyright (c) 2019
 * 
 */

#ifndef _FUNCTIONS_H_
#define _FUNCTIONS_H_

#include "Define_PointCloud_Algorithm.h"


enum ObjectType
{
	Cube,
	Cylinder,
	Other
};

PCXYZ_Ptr ReconstructSurface(PCXYZ_Ptr Source, float SearchRadius = 0.010);
PCXYZ_Ptr ReconstructSurface_FillHole(PCXYZ_Ptr Source, float SearchRadius = 0.010);

int RecognitionModel(PCXYZ_Ptr Model, PCXYZ_Ptr Scene, stdVector_Mat4f &Tran, std::vector<pcl::Correspondences> &clustered_corrs, RegnSetting Setting);







// base function
// OK

// type_convention


// calculation



#endif // !_FUNCTIONS_H_

