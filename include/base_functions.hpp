#ifndef __COOKTEAM_BASE_FUNCTION_H__
#define __COOKTEAM_BASE_FUNCTION_H__

#include "types.hpp"
#include "base_include.hpp"
#include "calculation.hpp"
#include "debug.hpp"
#include "type_convention.hpp"

#include "filters.hpp"

namespace CookTeam
{
    PCXYZ_Ptr Extract_FromIndices(PCXYZ_Ptr Data, PCIDX_Ptr idx);

    void RemovePoints_byModelAABB(PCXYZ_Ptr Source, PCXYZ_Ptr Model, PCXYZ_Ptr Output, double Scale = 1.0);  // TODO: Remove AABB calculation code.
    void RemovePoints_byAABB(PCXYZ_Ptr Source, PointXYZ &min, PointXYZ &max, PCXYZ_Ptr Output, double Scale = 1.0);
    void RemovePoints_byNeartest(PCXYZ_Ptr Source, PCXYZ_Ptr Model, PCXYZ_Ptr OutPut, PCXYZ_Ptr OutPut_Neg = PCXYZ_Ptr(new PCXYZ), double radius = 0.01);
    std::vector<float> GetNearPoints_Radius(PCXYZ_Ptr Source, PointXYZ Point, PCXYZ_Ptr &OutPut, double radius = 0.01, bool NeedRest = false, PCXYZ_Ptr OutPut_Neg = PCXYZ_Ptr(new PCXYZ));
    std::vector<float> GetNearPoints_NearestNum(PCXYZ_Ptr Source, PointXYZ Point, PCXYZ_Ptr &OutPut, int NearestNum = 1, bool NeedRest = false, PCXYZ_Ptr OutPut_Neg = PCXYZ_Ptr(new PCXYZ));
    void GetPoints_Near2PointsLine(PCXYZ_Ptr Source, PointXYZ P1, PointXYZ P2, PCXYZ_Ptr &OutPut, double radius = 0.004, double delta = 0.0005);

    PCXYZ_Ptr TransformPC_viaPoint(PCXYZ_Ptr Data, Vec3f Point, Eigen::Affine3f Rotation);
    PCXYZ_Ptr TransformPC_viaPoint(PCXYZ_Ptr Data, PointXYZ Point, Eigen::Affine3f Rotation);
}
#endif