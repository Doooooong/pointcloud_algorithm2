#ifndef __PCL_FUNC_TYPES_H__
#define __PCL_FUNC_TYPES_H__

#include "base_include.hpp"

//////// Eigen types
typedef Eigen::Matrix4f Mat4f;
typedef Eigen::Matrix4d Mat4d;
typedef Eigen::Matrix3f Mat3f;
typedef Eigen::Matrix3d Mat3d;
typedef Eigen::Matrix<float, 6, 1> Vector6f;
typedef Eigen::Matrix<float, 7, 1> Vector7f;
typedef Eigen::Vector3f Vec3f;
typedef Vector6f Vec6f;
typedef Vector7f Vec7f;
typedef std::vector<Eigen::Matrix4f, Eigen::aligned_allocator<Eigen::Matrix4f> > stdVector_Mat4f;

//////// PCL types
typedef pcl::PointXY									PointXY;
typedef pcl::PointXYZ									PointXYZ;
typedef pcl::PointNormal								PointXYZN;
typedef pcl::PointXYZRGB								PointXYZRGB;
typedef pcl::PointXYZRGBNormal							PointXYZRGBN;
typedef pcl::PointCloud<pcl::PointXY>					PCXY;  // Point cloud type with point type : PointXY (x, y).
typedef pcl::PointCloud<pcl::PointXYZ>					PCXYZ;  // Point cloud type with point type : PointXYZ (x, y, z).
typedef pcl::PointCloud<pcl::PointNormal>				PCXYZN;  // Point cloud type with point type : PointNormal (x, y, z, nx, ny, nz, curvature).
typedef pcl::PointCloud<pcl::PointXYZRGB>				PCXYZRGB;  // Point cloud type with point type : PointXYZRGB (x, y, z, r, g, b).
typedef pcl::PointCloud<pcl::PointXYZRGBNormal>			PCXYZRGBN;  // Point cloud type with point type : PointXYZRGBNormal (x, y, z, r, g, b, nx, ny, nz, curvature).
typedef std::shared_ptr<pcl::PointCloud<pcl::PointXY> >				PCXY_Ptr;  // pcl::PointCloud\<pcl::PointXY>::Ptr
typedef std::shared_ptr<pcl::PointCloud<pcl::PointXYZ> >				PCXYZ_Ptr;  // pcl::PointCloud\<pcl::PointXYZ>::Ptr
typedef std::shared_ptr<pcl::PointCloud<pcl::PointNormal> >			PCXYZN_Ptr;  // pcl::PointCloud\<pcl::PointNormal>::Ptr
typedef std::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB> >			PCXYZRGB_Ptr;  // pcl::PointCloud\<pcl::PointXYZRGB>::Ptr
typedef std::shared_ptr<pcl::PointCloud<pcl::PointXYZRGBNormal> >		PCXYZRGBN_Ptr;  // pcl::PointCloud\<pcl::PointXYZRGBNormal>::Ptr
typedef pcl::PointIndices			PCIDX;  // A set of index of a point cloud.
typedef pcl::PointIndices::Ptr		PCIDX_Ptr;  // The smart pointer type to PCIDX.
typedef pcl::PointCloud<pcl::Normal> PCN;  // Only have curvature of surface of point cloud?????, but this type had been used in RecognitionModel and FindCorres_WithSHOTFeature function.
typedef pcl::PointCloud<pcl::Normal>::Ptr PCN_Ptr;

//////// PCL PLY Mesh
#include <pcl/geometry/polygon_mesh.h>
typedef pcl::PolygonMesh PlyMesh;
typedef pcl::PolygonMesh::Ptr PlyMesh_Ptr;

namespace CookTeam
{
//////// PCL struct
typedef struct RecognizeSettingStruct
{
	uint32_t NormalKSearch = 15;

	bool BOARD_RF_FindHole = true;
	float BOARD_RF_RadiusSearch = 0.015f;

	float HoughBinSize = 0.01f;
	float HoughThreshold = 5.0f;
	bool HoughUseInterpolation = true;
	bool HoughUseDistanceWeight = false;
}RegnSetting;


template <typename T>
constexpr bool is_PointXY_type = std::is_same<T, PointXY>::value;
template <typename T>
constexpr bool is_Point_type = std::is_same<T, PointXY>::value || 
	std::is_same<T, PointXY>::value || 
	std::is_same<T, PointXYZ>::value || 
	std::is_same<T, PointXYZN>::value || 
	std::is_same<T, PointXYZRGB>::value || 
	std::is_same<T, PointXYZRGBN>::value;
template <typename T>
constexpr bool is_Point_Normal_type = 
	std::is_same<T, PointXYZN>::value ||  
	std::is_same<T, PointXYZRGBN>::value;
template <typename T>
constexpr bool is_Point_RGB_type = 
	std::is_same<T, PointXYZRGB>::value || 
	std::is_same<T, PointXYZRGBN>::value;

template <typename T>
constexpr bool is_PC_type = std::is_same<T, PCXY>::value || 
	std::is_same<T, PCXY>::value || 
	std::is_same<T, PCXYZ>::value || 
	std::is_same<T, PCXYZN>::value || 
	std::is_same<T, PCXYZRGB>::value || 
	std::is_same<T, PCXYZRGBN>::value;
template <typename T>
constexpr bool is_PC_Normal_type = 
	std::is_same<T, PCXYZN>::value ||  
	std::is_same<T, PCXYZRGBN>::value;
template <typename T>
constexpr bool is_PC_RGB_type = 
	std::is_same<T, PCXYZRGB>::value || 
	std::is_same<T, PCXYZRGBN>::value;

template <typename T>
constexpr bool is_PCptr_type = std::is_same<T, PCXY_Ptr>::value || 
	std::is_same<T, PCXY_Ptr>::value || 
	std::is_same<T, PCXYZ_Ptr>::value || 
	std::is_same<T, PCXYZN_Ptr>::value || 
	std::is_same<T, PCXYZRGB_Ptr>::value || 
	std::is_same<T, PCXYZRGBN_Ptr>::value;
template <typename T>
constexpr bool is_PCptr_Normal_type = 
	std::is_same<T, PCXYZN_Ptr>::value ||  
	std::is_same<T, PCXYZRGBN_Ptr>::value;
template <typename T>
constexpr bool is_PCptr_RGB_type = 
	std::is_same<T, PCXYZRGB_Ptr>::value || 
	std::is_same<T, PCXYZRGBN_Ptr>::value;
}

#endif