#ifndef __COOKTEAM_APP_H__
#define __COOKTEAM_APP_H__

#include "types.hpp"
#include "base_functions.hpp"

#include "filters.hpp"

#include "pcl/surface/convex_hull.h"

namespace CookTeam
{
	typedef struct PC_SliceStruct
	{
		float StartHeight = 0.f;
		float Thickness = 0.f;
		PCXYZ_Ptr Sliced_PC = PCXYZ_Ptr(new PCXYZ);
	}PC_Slice;
	int SlicePointCloud(PCXYZ_Ptr Data, PCXYZ_Ptr SlicedPC[], PC_Slice res[], float Thickness = 0.002f, int MinPoints = 20);
	int SlicePointCloud2(PCXYZ_Ptr Data, PCXYZ_Ptr SlicedPC[], PC_Slice res[], float Thickness = 0.002f, int MinPoints = 20, float CutoffHeight = INFINITY);
	double VolumeCalculation_byZAxisSlice(PCXYZ_Ptr Model, Vec6f AABB = Vec6f::Zero(), float LayerHeight = 0.001f);
}

#endif