#ifndef __COOKTEAM_FILTERS_H__
#define __COOKTEAM_FILTERS_H__

#include "types.hpp"

#include <pcl/filters/passthrough.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/convolution_3d.h>
#include <pcl/filters/fast_bilateral.h>

namespace CookTeam
{
// Const type variables for Filters() function.
const int Filter_FastBilateral = 0x01; // FastBilateralFilter
const int Filter_Voxel = 0x02;		   // VoxelGrid
const int Filter_Pass = 0x04;		   // PassFilter
const int Filter_Stat = 0x08;		   // StatisticRemover

/**
	 * @brief 
	 */
typedef struct FiltersSettingStruct
{
	std::string FilterFieldName = "z";
	float FilterLimitsMin = 0.0f;
	float FilterLimitsMax = 1.0f;

	float LeafSizeX = 0.05f;
	float LeafSizeY = 0.05f;
	float LeafSizeZ = 0.05f;

	uint32_t MeanK = 50;
	float StddevMulThresh = 0.6f;

	float SigmaS = 1;
	float SigmaR = 0.02;
} FiltersSetting;
/**
	 * @brief Apply four basic filters to point cloud, for point cloud pre-processing.
	 * 
	 * @param Source : Point cloud pointer that want to process.
	 * @param Output : Result point cloud pointer.
	 * @param Setting : Settings for each filter. {@link FiltersSetting}
	 * @param FilterMask : Mask for specify which filter will be applied.
	 * @return PCXYZ_Ptr Result point cloud pointer .
	 * 
	 * @sa: <a href="http://pointclouds.org/documentation/tutorials/voxel_grid.php#voxelgrid">Voxel Grid Filter</a>, 
	 * <a href="http://pointclouds.org/documentation/tutorials/passthrough.php#passthrough">Pass Through Filter</a>, 
	 * <a href="http://pointclouds.org/documentation/tutorials/statistical_outlier.php#statistical-outlier-removal">Statistical Outlier Removal</a>, 
	 * <a href="http://docs.pointclouds.org/trunk/classpcl_1_1_bilateral_filter.html">Bilateral Filter</a>, 
	 * 
	 * @caution: BilateralFilter need struct point cloud(point cloud that storaged like 2-D depth image), we can not get struct point cloud, so this filter is useless.
	 * 
	 * @code:
	 * Filters(read, result, filter_setting, Filter_Voxel | Filter_Pass | Filter_Stat);  // Apply only [VoxelGrid],[PassThrough],[StatisticalOutlierRemoval] filters.
	 * @endcode
	 */
PCXYZ_Ptr Filters(PCXYZ_Ptr Source, PCXYZ_Ptr &Output, FiltersSetting Setting, uint8_t FilterMask = 0x0E);

PCXYZ_Ptr PassThrough(PCXYZ_Ptr Source, std::string axis = "z", float min = INFINITY, float max = INFINITY);
PCXYZ_Ptr PassThrough(PCXYZ_Ptr Source, std::string axis = "z", Vec3f min = Vec3f(-INFINITY, -INFINITY, -INFINITY), Vec3f max = Vec3f(INFINITY, INFINITY, INFINITY));

PCXYZ_Ptr VoxelFilter(PCXYZ_Ptr Source, Vec3f voxel_size = Vec3f(0.002, 0.002, 0.002));
PCXYZ_Ptr StatisticRemoval(PCXYZ_Ptr Source, uint32_t NumNeighbors=25, float StdVar=0.6);

} // namespace CookTeam

#endif