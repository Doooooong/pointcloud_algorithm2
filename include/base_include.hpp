#ifndef __COOKTEAM_BASE_INCLUDE_H__
#define __COOKTEAM_BASE_INCLUDE_H__

#include <iostream>
#include <type_traits>

#include <omp.h>
#include <tbb/tbb.h>

#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Geometry>
#include <eigen3/Eigen/Dense>

#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_cloud.h>
#include <pcl/console/parse.h>

#include <pcl/common/transforms.h>

#include <pcl/filters/extract_indices.h>

#include <pcl/features/normal_3d.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/kdtree/kdtree.h>

namespace CookTeam
{
    template <typename T>
    constexpr bool is_number_type = std::is_arithmetic<T>::value;
}

#endif