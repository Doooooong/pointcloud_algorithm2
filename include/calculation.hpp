#ifndef __COOKTEAM_CALCULATION_H__
#define __COOKTEAM_CALCULATION_H__

#include "types.hpp"
#include "filters.hpp"

#include <pcl/common/pca.h>

namespace CookTeam
{
extern const double TO_RAD;
extern const double TO_DEG;

/* ********************************** */
/*          Inline functions          */
/* ********************************** */
float MeanFilter(int Size, float NewData);
PointXYZ ProjectPoint2Line(PointXYZ P1, PointXYZ P2, PointXYZ ProjectPoint, float &Percent);

/* ********************************** */
/*           PCA functions            */
/* ********************************** */
/**
 * @brief 
 * 
 */
typedef struct PCAOutStruct
{
	PCXYZ_Ptr ProjectPC;
	Mat4f TransformMatrix;
	Eigen::Matrix3f Rotation;
	Eigen::Vector3f Transform;
	Vector6f xyzabc;
} PCAOut;

void Change_PCA_CoordinateOrder(Mat4f &Data, int Principle1Axis, int Principle2Axis, bool isRightHandCoordinate, float SecondPrincpleAngle = 999.0f);
PCAOut Cal_PCA(PCXYZ_Ptr Source, std::string CoordinateOrder = "+x+y", bool NeedProjection = false, bool isRightHandCoordinate = true);

/* ********************************** */
/*          Other functions           */
/* ********************************** */
PCXYZ_Ptr Compress_PC(PCXYZ_Ptr Source, std::string AxisReserve = "xy", float Pos1 = 0.0f, float Pos2 = 0.0f);
PCXYZ_Ptr Compress_PC_MP(PCXYZ_Ptr Source, std::string AxisReserve = "xy", float Pos1 = 0.0f, float Pos2 = 0.0f);

// AABB(Axis-Aligned-Bounding-Box) calculation functions.
void Cal_AABB(PCXYZ_Ptr Source, PointXYZ &min, PointXYZ &max);
void Cal_AABB_MP(PCXYZ_Ptr Source, PointXYZ &min, PointXYZ &max);
Vec6f Cal_AABB(PCXYZ_Ptr Source);
Vec6f Cal_AABB_MP(PCXYZ_Ptr Source);

float Cal_AABB_Volume(PointXYZ &min, PointXYZ &max);

PointXYZ Cal_Center_MP(PCXYZ_Ptr Source, PointXYZ &center);
PointXYZ Cal_Center(PCXYZ_Ptr Source, PointXYZ &center);

PCXYZN_Ptr Cal_Normal_MP(PCXYZ_Ptr Source, PCXYZN_Ptr &out, PCN_Ptr &normal, double radius=0.03);

void Get_MinMax_Point(PCXYZ_Ptr Source, PointXYZ &min, PointXYZ &max, std::string axis = "x");
void Get_MinMax_Point_MP(PCXYZ_Ptr Source, PointXYZ &min, PointXYZ &max, std::string axis = "x");

Eigen::Vector4f Analyze_Dir_Center(PCXYZ_Ptr Source, Mat4f PCA_Result_Matrix); // TODO: Maybe write into app.

Mat3f CalRotationMatrix_byDirectionVec(Eigen::Vector3f Dir, Eigen::Vector3f Ref = Eigen::Vector3f::UnitZ());
Mat3f CalRotationMatrix_byDirectionVec(Eigen::Vector3f Dir, char axis1, Eigen::Vector3f Ref, char axis2);

/* ********************************** */
/*          Inline functions          */
/* ********************************** */
inline PointXYZ operator+(PointXYZ l, PointXYZ r)
{
	PointXYZ res;
	res.x = l.x + r.x;
	res.y = l.y + r.y;
	res.z = l.z + r.z;
	return res;
}
inline PointXYZ operator-(PointXYZ l, PointXYZ r)
{
	PointXYZ res;
	res.x = l.x - r.x;
	res.y = l.y - r.y;
	res.z = l.z - r.z;
	return res;
}
template <typename T>
inline PointXYZ operator/(PointXYZ l, T r)
{
	PointXYZ res;
	res.x = l.x / (float)r;
	res.y = l.y / (float)r;
	res.z = l.z / (float)r;
	return res;
}
inline float operator*(PointXYZ l, PointXYZ r)
{
	return l.x * r.x + l.y * r.y + l.z * r.z;
}
template <typename T>
inline PointXYZ operator*(PointXYZ l, T r)
{
	PointXYZ res;
	res.x = l.x * (float)r;
	res.y = l.y * (float)r;
	res.z = l.z * (float)r;
	return res;
}
inline bool operator==(PointXYZ l, PointXYZ r)
{
	PointXYZ res = l - r;
	if ((abs(res.x) + abs(res.y) + abs(res.z)) < 1e-8)
		return true;
	else
		return false;
}
inline bool operator!=(PointXYZ l, PointXYZ r)
{
	PointXYZ res = l - r;
	if ((abs(res.x) + abs(res.y) + abs(res.z)) > 1e-8)
		return true;
	else
		return false;
}
inline bool operator==(Vec3f l, Vec3f r)
{
	Vec3f res = l - r;
	if ((abs(res(0)) + abs(res(1)) + abs(res(2))) < 1e-8)
		return true;
	else
		return false;
}

inline float CalVector3Angle(Vec3f data1, Vec3f data2)
{
	return data1.dot(data2) / (data1.dot(data1) * data2.dot(data2));
}
inline float CalDistance(Vec3f p1, Vec3f p2, bool is2D = false)
{
	if (!is2D)
		return std::sqrt(std::pow(p1[0] - p2[0], 2) + std::pow(p1[1] - p2[1], 2) + std::pow(p1[2] - p2[2], 2));
	else
		return std::sqrt(std::pow(p1[0] - p2[0], 2) + std::pow(p1[1] - p2[1], 2));
}
inline float CalDistance(PointXYZ p1, PointXYZ p2, bool is2D = false)
{
	if (!is2D)
		return std::sqrt(std::pow(p1.x - p2.x, 2) + std::pow(p1.y - p2.y, 2) + std::pow(p1.z - p2.z, 2));
	else
		return std::sqrt(std::pow(p1.x - p2.x, 2) + std::pow(p1.y - p2.y, 2));
}
inline float CalDistance(PointXY p1, PointXY p2, bool is2D = false)
{
	return std::sqrt(std::pow(p1.x - p2.x, 2) + std::pow(p1.y - p2.y, 2));
}

/**
 * @brief Calculate the third axis direction using cross product. This function intend to make a coordinate axis vector.
 * 
 * @param LeftVec Left vector in cross product.
 * @param RightVec Right vector in cross product
 * @param isRightHandCoordinate If false the LEFT hand coordinate will be calculated as result and vice versa.
 * @return Vec3f Result vector with Eigen::Vec3f type.
 */
inline Vec3f Cal_ThirdAxisDir(Vec3f LeftVec, Vec3f RightVec, bool isRightHandCoordinate)
{
	if (isRightHandCoordinate)
		return LeftVec.cross(RightVec);
	else
		return -LeftVec.cross(RightVec);
}

} // namespace CookTeam
#endif