#ifndef __COOKTEAM_SEGMENT_H__
#define __COOKTEAM_SEGMENT_H__


#include "types.hpp"
#include "base_functions.hpp"
#include "calculation.hpp"

#include <pcl/segmentation/extract_clusters.h>
#include <pcl/segmentation/region_growing.h>

#include <pcl/gpu/segmentation/gpu_extract_clusters.h>
#include <pcl/gpu/octree/octree.hpp>


namespace CookTeam
{
	/**
	 * @brief The settings for Euclidean Clustering.
	 */
	typedef struct EuclideanClusterSettingStruct
	{
		float ClusterTolerance = 0.01f;  ///< The distance threashold between two clusters.
		uint32_t MinClusterSize;  ///< Min number of points in result clusters.
		uint32_t MaxClusterSize;  ///< Max number of points in result clusters. The number of points$p$ in final result clusters is [MinClusterSize \f$<p<\f$ MaxClusterSize].
	}EC_Setting;

	typedef struct RegionGrowCLusterSettingStruct
	{
		float		SmoothnessThreshold = (10.0 * TO_RAD);
		float		CurvatureThreshold = (1.0);
		int			NormalKSearch = 50;
		int			NumberOfNeighbours = 30;
		uint32_t	MinClusterSize;
		uint32_t	MaxClusterSize;
	}RGC_Setting;

	int ExtractEuclideanCluster(PCXYZ_Ptr Source, PCXYZ_Ptr Clusters[], EC_Setting Setting, bool ClusterWithoutZAxis = false);
	int ExtractRegionGrowCLuster(PCXYZ_Ptr Source, PCXYZ_Ptr Clusters[], RGC_Setting Setting);
}

#endif