#include "calculation.hpp"
#include "debug.hpp"

namespace CookTeam
{
const double TO_RAD = (3.14159265359 / 180.0);
const double TO_DEG = (180.0 / 3.14159265359);

float MeanFilter(int Size, float NewData)
{
    static float *buffer = new float[Size];
    static bool isFirst = true;
    static int FilterSize = Size;
    static float Sum = 0;
    static uint64_t count = 0;
    static int Head = count % FilterSize;

    if (isFirst)
    {
        for (int i = 0; i < Size; i++)
            buffer[i] = 0;
        isFirst = false;
    }

    Head = count % FilterSize;

    Sum = Sum - buffer[Head] + NewData;

    // Std_Out("buffer[Head]: ", buffer[Head], "NewData: ", NewData, "Sum: ", Sum);
    buffer[Head] = NewData;
    count++;

    return Sum / FilterSize;
}

PointXYZ ProjectPoint2Line(PointXYZ P1, PointXYZ P2, PointXYZ ProjectPoint, float &Percent)
{
    float Distance = CalDistance(P1, P2);
    PointXYZ LineDirectVector = P2 - P1;
    float ProjectLength = ((ProjectPoint - P1) * LineDirectVector) / (Distance);

    Percent = ProjectLength / Distance;
    return P1 + LineDirectVector * Percent;
}

/**
     * @brief This function be used for {@link PCA_Cal} function. It intend to correct the rotation matrix that be calculated by {@link PCA_Cal} function with specified axis order and . 
     * 
     * @param Data The rotation matrix that you want correct.
     * @param Principle1Axis Direction of first. Use number with [-3, -2, -1, 1, 2, 3] to [-z, -y, -x, x, y, z]
     * @param Principle1Axis Direction of first. Use number with [-3, -2, -1, 1, 2, 3] to [-z, -y, -x, x, y, z]
     * @param isRightHandCoordinate 
     * @param SecondPrincpleAngle NOT USE!!!
     */
void Change_PCA_CoordinateOrder(Mat4f &Data, int Principle1Axis, int Principle2Axis, bool isRightHandCoordinate, float SecondPrincpleAngle)
{
    int Principle1AxisPos = abs(Principle1Axis);
    int Principle2AxisPos = abs(Principle2Axis);

    assert((Principle1Axis >= -3 && Principle1Axis <= 3 && Principle1Axis != 0) && "In function [Change_PCA_CoordinateOrder]. Argument [Principle1Axis] input the invalid value!");
    assert((Principle2Axis >= -3 && Principle2Axis <= 3 && Principle2Axis != 0) && "In function [Change_PCA_CoordinateOrder]. Argument [Principle2Axis] input the invalid value!");
    assert((Principle1Axis != Principle2Axis) && "In function [Change_PCA_CoordinateOrder]. Argument [Principle2Axis] can't equal to [Principle1Axis]!");

    Vec3f Principle1 = Data.block<3, 1>(0, 0);
    Vec3f Principle2 = Data.block<3, 1>(0, 1);
    Vec3f Principle3 = Data.block<3, 1>(0, 2);

    // Reform matrix to axis specific by users.
    Data.block<3, 1>(0, Principle1AxisPos - 1) = Principle1;
    Data.block<3, 1>(0, Principle2AxisPos - 1) = Principle2;
    Data.block<3, 1>(0, 6 - Principle1AxisPos - Principle2AxisPos - 1) = Principle3;

    // Create axis vectors into array.
    Vec3f PositiveDirs[3];
    PositiveDirs[0] = Vec3f(1, 0, 0);
    PositiveDirs[1] = Vec3f(0, 1, 0);
    PositiveDirs[2] = Vec3f(0, 0, 1);
    // Fix direction of first Principle.
    // if signal of Principle1Axis and signal of dot product (PositiveDirs and Principle direction) is not same, inverse the direction.
    if (Principle1.dot(PositiveDirs[Principle1AxisPos - 1]) * Principle1Axis < 0)
        Data.block<3, 1>(0, Principle1AxisPos - 1) = -Principle1;
    if (Principle2.dot(PositiveDirs[Principle2AxisPos - 1]) * Principle2Axis < 0)
        Data.block<3, 1>(0, Principle2AxisPos - 1) = -Principle2;

    Vec3f Axis_X = Data.block<3, 1>(0, 0);
    Vec3f Axis_Y = Data.block<3, 1>(0, 1);
    Vec3f Axis_Z = Data.block<3, 1>(0, 2);
    // Correct the type of coordinate (Left hand coordinate or right hand coordinate).
    switch (Principle1AxisPos + Principle2AxisPos)
    {
    case 1 + 2: // x and y.
    {
        Data.block<3, 1>(0, 2) = Cal_ThirdAxisDir(Axis_X, Axis_Y, isRightHandCoordinate);
        break;
    }
    case 1 + 3: // x and z.
    {
        Data.block<3, 1>(0, 1) = Cal_ThirdAxisDir(Axis_Z, Axis_X, isRightHandCoordinate);
        break;
    }
    case 2 + 3: // y and z.
    {
        Data.block<3, 1>(0, 0) = Cal_ThirdAxisDir(Axis_Y, Axis_Z, isRightHandCoordinate);
        break;
    }
    }
}

/**
     * @brief Calculate the PCA(Principle Component Analysis) of provided point cloud.
     * @param Source: Point cloud you want to calculate.
     * @param CoordinateOrder: (Default: "+x+y")Select axis and direction that fix to first and second Principle Component vector. You should input string value like "+x+y" to specific axis and direction. Direction: "+" means the dot product of this direction and UnitZ(0,0,1) is plus. For example: "-z+y" means
     * @param NeedProjection: (Default: false), If true, PCA will calculate point cloud that transformed into eigenspace into PCAOut.ProjectPC.
     * @param isRightHandCoordinate: (Default: true) Default is right hand coordinate. If false, it'll calculate in left hand coordinate.
     * @return PCAOut structor with all of PCA result.
     */
PCAOut Cal_PCA(PCXYZ_Ptr Source, std::string CoordinateOrder, bool NeedProjection, bool isRightHandCoordinate)
{
    // Main code.
    PCAOut Res;
    pcl::PCA<PointXYZ> PCA_Calculator(new pcl::PCA<PointXYZ>);
    PCA_Calculator.setInputCloud(Source);
    // When finished PCA_Calculator.setInputCloud, PCA calculation is finished. You can call PCA_Calculator.getEigenVectors() to obtain eigenvectors.
    Eigen::Vector4f pos = PCA_Calculator.getMean();
    Eigen::Translation3f tran(pos(0), pos(1), pos(2));
    Res.Transform = Vec3f(pos(0), pos(1), pos(2));
    // Result process.
    // Make transform matrix that from eigenspace to world space.
    Eigen::Affine3f tmp(tran);
    // Eigen::Affine3f tmp(Eigen::Translation3f(Res.Transform = PCA_Calculator.getMean().block<3, 1>(0, 0))); // Translate part.
    tmp.rotate(Res.Rotation = PCA_Calculator.getEigenVectors());                                           // Rotation part

    // Calculate transform matrix and transform part.
    Res.TransformMatrix = tmp.matrix();

    // Process direction and CoordinateOrder specific by user. Deal with right hand coordinate.
    int Principle1Axis = (-(CoordinateOrder[0] - ',')) * (CoordinateOrder[1] - 'w'); // Analyze CoordinateOrder
    int Principle2Axis = (-(CoordinateOrder[2] - ',')) * (CoordinateOrder[3] - 'w'); // ascii code of ',' is between '+' and '-', and '+' is lower.
    Change_PCA_CoordinateOrder(Res.TransformMatrix, Principle1Axis, Principle2Axis, isRightHandCoordinate, 0);

    // Calculate xyzabc.
    Res.xyzabc.block<3, 1>(0, 0) = Res.Transform;
    Res.xyzabc.block<3, 1>(3, 0) = Res.Rotation.eulerAngles(2, 1, 0);

    if (NeedProjection)
    {
        Res.ProjectPC = PCXYZ_Ptr(new PCXYZ);
        PCA_Calculator.project(*Source, *Res.ProjectPC);
    }
    return Res;
}

/**
     * @brief Reserve 1 or 2 axis data of points.
     * 
     * @note This function project the point cloud to the XY, YZ, ZX plane or only to x-axis, y-axis, z-axis.
     * For example, XY plane: Coordinate of z in [Source] is change to [Pos1], x and y is not change; XZ plane: Coordinate of y in [Source] is change to [Pos1], x and z is not change. x-axis: Coordinate of x and y is [Pos1] and [Pos2], z is not change.
     * | AxisReserve | x | y | z |
     * | ----------- | ---- | ---- | ---- |
     * | INPUT | OUTPUT |||
     * | "xy" or "yx" | Not Change | Not Change | Pos1 |
     * | "xz" or "zx" | Not Change | Pos1 | Not Change |
     * | "yz" or "zy" | Pos1 | Not Change | Not Change |
     * | "x" | Not Change | Pos1 | Pos2 |
     * | "y" | Pos1 | Not Change | Pos2 |
     * | "z" | Pos1 | Pos2 | Not Change |
     * 
     * @param Source Point cloud you want to project.
     * @param AxisReserve Only accept string in ["xy", "yx", "xz", "zx", "yz", "zy", "x", "y", "z"].
     * @param Pos1 The fixed value assigned to x y or z coordinate
     * @param Pos2 
     * @return PCXYZ_Ptr 
     */
PCXYZ_Ptr Compress_PC(PCXYZ_Ptr Source, std::string AxisReserve, float Pos1, float Pos2)
{
    PCXYZ_Ptr tmp = PCXYZ_Ptr(new PCXYZ);
    if (AxisReserve.length() == 3)
        return Source;
    else if (AxisReserve.length() == 0)
        return tmp;

    if (AxisReserve == "x")
    {
        for (PointXYZ point : Source->points)
        {
            point.y = Pos1;
            point.z = Pos2;
            tmp->points.push_back(point);
        }
    }
    else if (AxisReserve == "y")
    {
        for (PointXYZ point : Source->points)
        {
            point.x = Pos1;
            point.z = Pos2;
            tmp->points.push_back(point);
        }
    }
    else if (AxisReserve == "z")
    {
        for (PointXYZ point : Source->points)
        {
            point.x = Pos1;
            point.y = Pos2;
            tmp->points.push_back(point);
        }
    }
    else if (AxisReserve == "xy" || AxisReserve == "yx")
    {
        for (PointXYZ point : Source->points)
        {
            point.z = Pos1;
            tmp->points.push_back(point);
        }
    }
    else if (AxisReserve == "xz" || AxisReserve == "zx")
    {
        for (PointXYZ point : Source->points)
        {
            point.y = Pos1;
            tmp->points.push_back(point);
        }
    }
    else if (AxisReserve == "zy" || AxisReserve == "yz")
    {
        for (PointXYZ point : Source->points)
        {
            point.x = Pos1;
            tmp->points.push_back(point);
        }
    }
    return tmp;
}
/**
 * @brief The multi-processing optimized version, using intel TBB, 30% speed up, see more details about parameters: {@link CompressPC}
 */
PCXYZ_Ptr Compress_PC_MP(PCXYZ_Ptr Source, std::string AxisReserve, float Pos1, float Pos2)
{
    PCXYZ_Ptr tmp = PCXYZ_Ptr(new PCXYZ);
    if (AxisReserve.length() == 3)
        return Source;
    else if (AxisReserve.length() == 0)
        return tmp;

    tmp->points.reserve(Source->size());
    tmp->points.resize(Source->size());
    if (AxisReserve == "x")
    {
        tbb::parallel_for(tbb::blocked_range<size_t>(0, Source->size()), [&](const tbb::blocked_range<size_t> &r) {
            for (size_t i = r.begin(); i != r.end(); ++i)
            {
                tmp->points[i].x = Source->points[i].x;
                tmp->points[i].y = Pos1;
                tmp->points[i].z = Pos2;
            }
        });
    }
    else if (AxisReserve == "y")
    {
        tbb::parallel_for(tbb::blocked_range<size_t>(0, Source->size()), [&](const tbb::blocked_range<size_t> &r) {
            for (size_t i = r.begin(); i != r.end(); ++i)
            {
                tmp->points[i].y = Source->points[i].y;
                tmp->points[i].x = Pos1;
                tmp->points[i].z = Pos2;
            }
        });
    }
    else if (AxisReserve == "z")
    {
        tbb::parallel_for(tbb::blocked_range<size_t>(0, Source->size()), [&](const tbb::blocked_range<size_t> &r) {
            for (size_t i = r.begin(); i != r.end(); ++i)
            {
                tmp->points[i].x = Pos1;
                tmp->points[i].y = Pos2;
                tmp->points[i].z = Source->points[i].z;
            }
        });
    }
    else if (AxisReserve == "xy" || AxisReserve == "yx")
    {
        tbb::parallel_for(tbb::blocked_range<size_t>(0, Source->size()), [&](const tbb::blocked_range<size_t> &r) {
            for (size_t i = r.begin(); i != r.end(); ++i)
            {
                tmp->points[i].x = Source->points[i].x;
                tmp->points[i].y = Source->points[i].y;
                tmp->points[i].z = Pos1;
            }
        });
    }
    else if (AxisReserve == "xz" || AxisReserve == "zx")
    {
        tbb::parallel_for(tbb::blocked_range<size_t>(0, Source->size()), [&](const tbb::blocked_range<size_t> &r) {
            for (size_t i = r.begin(); i != r.end(); ++i)
            {
                tmp->points[i].y = Pos1;
                tmp->points[i].x = Source->points[i].x;
                tmp->points[i].z = Source->points[i].z;
            }
        });
    }
    else if (AxisReserve == "zy" || AxisReserve == "yz")
    {
        tbb::parallel_for(tbb::blocked_range<size_t>(0, Source->size()), [&](const tbb::blocked_range<size_t> &r) {
            for (size_t i = r.begin(); i != r.end(); ++i)
            {
                tmp->points[i].x = Pos1;
                tmp->points[i].y = Source->points[i].y;
                tmp->points[i].z = Source->points[i].z;
            }
        });
    }
    return tmp;
}

/**
     * @brief Cal_AABB: AABB:Axis Aligned Bounding Box. Calculate bounding box that fixed with XYZ axis.
     * @param Source: Point cloud you want to calculate.
     * @param min: Reference storage XYZ axis max data.
     * @param max: Reference storage XYZ axis min data.
     * Output: Two points that XYZ axis max or min data had been written into.
     */
void Cal_AABB(PCXYZ_Ptr Source, PointXYZ &min, PointXYZ &max)
{
    min.x = INFINITY;
    min.y = INFINITY;
    min.z = INFINITY;
    max.x = -INFINITY;
    max.y = -INFINITY;
    max.z = -INFINITY;
    for (PointXYZ point : Source->points)
    {
        if (point.x > max.x)
            max.x = point.x;
        if (point.x < min.x)
            min.x = point.x;

        if (point.y > max.y)
            max.y = point.y;
        if (point.y < min.y)
            min.y = point.y;

        if (point.z > max.z)
            max.z = point.z;
        if (point.z < min.z)
            min.z = point.z;
    }
}
#ifdef __TESTING_CODE__
// Using MomentOfInertiaEstimation to calculate center and AABB is critical slow!
pcl::MomentOfInertiaEstimation<pcl::PointXYZ> feature_extractor;
feature_extractor.setInputCloud(Source);
feature_extractor.compute();
feature_extractor.getAABB(min, max);
#endif
void Cal_AABB_MP(PCXYZ_Ptr Source, PointXYZ &min, PointXYZ &max)
{
    min.x = INFINITY;
    min.y = INFINITY;
    min.z = INFINITY;
    max.x = -INFINITY;
    max.y = -INFINITY;
    max.z = -INFINITY;
    tbb::parallel_for(tbb::blocked_range<size_t>(0, Source->size()), [&](const tbb::blocked_range<size_t> &r) {
        for (size_t i = r.begin(); i != r.end(); ++i)
        {
            if (Source->points[i].x > max.x)
                max.x = Source->points[i].x;
            if (Source->points[i].x < min.x)
                min.x = Source->points[i].x;

            if (Source->points[i].y > max.y)
                max.y = Source->points[i].y;
            if (Source->points[i].y < min.y)
                min.y = Source->points[i].y;

            if (Source->points[i].z > max.z)
                max.z = Source->points[i].z;
            if (Source->points[i].z < min.z)
                min.z = Source->points[i].z;
        }
    });
}

Vec6f Cal_AABB(PCXYZ_Ptr Source)
{
    Vec6f res;
    PointXYZ min, max;
    Cal_AABB_MP(Source, min, max);
    res(0) = min.x;
    res(1) = min.y;
    res(2) = min.z;
    res(3) = max.x;
    res(4) = max.y;
    res(5) = max.z;
    return res;
}
Vec6f Cal_AABB_MP(PCXYZ_Ptr Source)
{
    Vec6f res;
    PointXYZ min, max;
    Cal_AABB_MP(Source, min, max);
    res(0) = min.x;
    res(1) = min.y;
    res(2) = min.z;
    res(3) = max.x;
    res(4) = max.y;
    res(5) = max.z;
    return res;
}

float Cal_AABB_Volume(PointXYZ &min, PointXYZ &max)
{
    return (max.x - min.x) * (max.y - min.z) * (max.z - min.z) * (100 * 100 * 100);  // convert to cm^3.
}

void Get_minPoint(PCXYZ_Ptr Source, PointXYZ &min, std::string axis)
{
    min.x = INFINITY;
    min.y = INFINITY;
    min.z = INFINITY;

    if (axis == "x")
    {
        for (PointXYZ point : Source->points)
        {
            if (point.x < min.x)
            {
                min.x = point.x;
                min.y = point.y;
                min.z = point.z;
            }
        }
    }
    if (axis == "y")
    {
        for (PointXYZ point : Source->points)
        {
            if (point.y < min.y)
            {
                min.x = point.x;
                min.y = point.y;
                min.z = point.z;
            }
        }
    }
    if (axis == "z")
    {
        for (PointXYZ point : Source->points)
        {
            if (point.z < min.z)
            {
                min.x = point.x;
                min.y = point.y;
                min.z = point.z;
            }
        }
    }
}

void Get_MinMax_Point(PCXYZ_Ptr Source, PointXYZ &min, PointXYZ &max, std::string axis)
{
    min.x = INFINITY;
    min.y = INFINITY;
    min.z = INFINITY;
    max.x = -INFINITY;
    max.y = -INFINITY;
    max.z = -INFINITY;

    if (axis == "x")
        for (PointXYZ p : *Source)
        {
            if (p.x < min.x)
                min = p;
            if (p.x > max.x)
                max = p;
        }

    else if (axis == "y")
        for (PointXYZ p : *Source)
        {
            if (p.y < min.y)
                min = p;
            if (p.y > max.y)
                max = p;
        }

    else if (axis == "z")
        for (PointXYZ p : *Source)
        {
            if (p.z < min.z)
                min = p;
            if (p.z > max.z)
                max = p;
        }
}
void Get_MinMax_Point_MP(PCXYZ_Ptr Source, PointXYZ &min, PointXYZ &max, std::string axis)
{
    min.x = INFINITY;
    min.y = INFINITY;
    min.z = INFINITY;
    max.x = -INFINITY;
    max.y = -INFINITY;
    max.z = -INFINITY;

    if (axis == "x")
        tbb::parallel_for(tbb::blocked_range<size_t>(0, Source->size()), [&](const tbb::blocked_range<size_t> &r) {
            for (size_t i = r.begin(); i != r.end(); ++i)
            {
                if (Source->points[i].x < min.x)
                    min = Source->points[i];
                if (Source->points[i].x > max.x)
                    max = Source->points[i];
            }
        });

    else if (axis == "y")
        tbb::parallel_for(tbb::blocked_range<size_t>(0, Source->size()), [&](const tbb::blocked_range<size_t> &r) {
            for (size_t i = r.begin(); i != r.end(); ++i)
            {
                if (Source->points[i].y < min.y)
                    min = Source->points[i];
                if (Source->points[i].y > max.y)
                    max = Source->points[i];
            }
        });

    else if (axis == "z")
        tbb::parallel_for(tbb::blocked_range<size_t>(0, Source->size()), [&](const tbb::blocked_range<size_t> &r) {
            for (size_t i = r.begin(); i != r.end(); ++i)
            {
                if (Source->points[i].z < min.z)
                    min = Source->points[i];
                if (Source->points[i].z > max.z)
                    max = Source->points[i];
            }
        });
}
#ifdef __TESTING_CODE__
// auto points = std::vector<PointXYZ>(Source->points.begin() + r.begin(), Source->points.begin() + r.end());  // Too slow.
for (PointXYZ p : boost::make_iterator_range(Source->points.begin() + r.begin(), Source->points.begin() + r.end())) // same speed with now.
{
    if (p.x < min.x)
        min = p;
    if (p.x > max.x)
        max = p;
}

#endif

class TBB_PC_Center_Calculator
{
    PCXYZ_Ptr Source;

public:
    PointXYZ center;
    void operator()(const tbb::blocked_range<size_t> &r)
    {
        PCXYZ_Ptr tmp_Source = Source;
        PointXYZ tmp_center = center;
        for (size_t i = r.begin(); i != r.end(); ++i)
        {
            tmp_center.x += tmp_Source->points[i].x;
            tmp_center.y += tmp_Source->points[i].y;
            tmp_center.z += tmp_Source->points[i].z;
        }
        center = tmp_center;
    }

    TBB_PC_Center_Calculator(TBB_PC_Center_Calculator &x, tbb::split) : Source(x.Source), center(0, 0, 0) {}

    void join(const TBB_PC_Center_Calculator &y) { center = y.center + center; }

    TBB_PC_Center_Calculator(PCXYZ_Ptr a) : Source(a), center(0, 0, 0)
    {
    }
};

PointXYZ Cal_Center_MP(PCXYZ_Ptr Source, PointXYZ &center)
{
    TBB_PC_Center_Calculator sf(Source);
    tbb::parallel_reduce(tbb::blocked_range<size_t>(0, Source->size()), sf);

    sf.center.x /= Source->size();
    sf.center.y /= Source->size();
    sf.center.z /= Source->size();

    center = sf.center;
    return sf.center;
}

PointXYZ Cal_Center(PCXYZ_Ptr Source, PointXYZ &center)
{
    center.x = 0;
    center.y = 0;
    center.z = 0;

    for (size_t i = 0; i != Source->size(); ++i)
    {
        center.x += Source->points[i].x;
        center.y += Source->points[i].y;
        center.z += Source->points[i].z;
    }

    center.x /= Source->size();
    center.y /= Source->size();
    center.z /= Source->size();

    return center;
}

PCXYZN_Ptr Cal_Normal_MP(PCXYZ_Ptr Source, PCXYZN_Ptr &out, PCN_Ptr &normal, double radius)
{
    out = PCXYZN_Ptr(new PCXYZN);
    normal = PCN_Ptr(new PCN);

    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>());
    pcl::NormalEstimationOMP<pcl::PointXYZ, pcl::Normal> ne;
    ne.setInputCloud(Source);
    ne.setSearchMethod(tree);
    ne.setRadiusSearch(radius);
    ne.compute(*normal);

    pcl::PointNormal np;
    for (int x = 0; x < Source->points.size(); x++)
    {
        PointXYZ p = Source->points[x];

        np.x = p.x;
        np.y = p.y;
        np.z = p.z;
        np.normal_x = normal->points[x].normal_x;
        np.normal_y = normal->points[x].normal_y;
        np.normal_z = normal->points[x].normal_z;
        np.curvature = normal->points[x].curvature;
        out->points.push_back(np);
    }
    return out;
}

Eigen::Vector4f Analyze_Dir_Center(PCXYZ_Ptr Source, Mat4f PCA_Result_Matrix)
{
    Eigen::Vector4f Res;
    PCXYZ_Ptr TranOut = PCXYZ_Ptr(new PCXYZ);
    PCXYZ_Ptr Big = PCXYZ_Ptr(new PCXYZ);
    pcl::transformPointCloud(*Source, *TranOut, (Mat4f)PCA_Result_Matrix.inverse()); //
    float Delta_X = 0.035;

    PointXYZ org_min_point_AABB;
    PointXYZ org_max_point_AABB;
    Cal_AABB_MP(TranOut, org_min_point_AABB, org_max_point_AABB);
    //	Eigen::Affine3f tmp1(Eigen::Translation3f(
    //							org_min_point_AABB.x,
    //							org_min_point_AABB.y,
    //							org_min_point_AABB.z));
    //	Eigen::Affine3f tmp2(Eigen::Translation3f(
    //							org_max_point_AABB.x,
    //							org_max_point_AABB.y,
    //							org_max_point_AABB.z));

    PCXYZ_Ptr PassThrough_min = PassThrough(TranOut, "x", org_min_point_AABB.x, org_min_point_AABB.x + Delta_X);

    PointXYZ min_point_AABB;
    PointXYZ max_point_AABB;
    Cal_AABB_MP(PassThrough_min, min_point_AABB, max_point_AABB);
    //	PC_Show_Stuck(PassThrough_min, "Little", tmp1, 0.07);

    PCXYZ_Ptr PassThrough_max = PassThrough(TranOut, "x", org_max_point_AABB.x - Delta_X, org_max_point_AABB.x);
    PointXYZ other_min_point_AABB;
    PointXYZ other_max_point_AABB;
    Cal_AABB_MP(PassThrough_max, other_min_point_AABB, other_max_point_AABB);

    pcl::PCA<PointXYZ> PCA_Calculator(new pcl::PCA<PointXYZ>);
    //	std::cout << fabs(min_point_AABB.y - min_point_AABB.y) << std::endl;
    //	std::cout << fabs(other_min_point_AABB.y - other_max_point_AABB.y) << std::endl;
    if (fabs(min_point_AABB.y - max_point_AABB.y) < fabs(other_min_point_AABB.y - other_max_point_AABB.y))
        PCA_Calculator.setInputCloud(PassThrough_max);
    else
        PCA_Calculator.setInputCloud(PassThrough_min);

    Eigen::Affine3f Transform(Eigen::Translation3f(
        PCA_Calculator.getMean().x(),
        PCA_Calculator.getMean().y(),
        PCA_Calculator.getMean().z()));
    Transform.rotate(PCA_Calculator.getEigenVectors());

    __DEBUG_PROBE__(PCA_Calculator.getEigenVectors().eulerAngles(0, 1, 2));
    Res[0] = PCA_Calculator.getMean().x();
    Res[1] = PCA_Calculator.getMean().y();
    Res[2] = PCA_Calculator.getMean().z();
    Res[3] = 1;

    Eigen::Vector4f Res2 = PCA_Result_Matrix * Res;
    std::cout << Res2 << std::endl;
    return Res2;
}

/**
     * @brief Calculate the rotation matrix (from right hand base coordinate) with Z-Axis point to [Dir], the X-Axis is point to the cross product of [Ref] and [Dir]
     * todo:
     * @param Dir 
     * @param Ref 
     * @return Mat3f Result in Eigen::Mat3f type.
     */
Mat3f CalRotationMatrix_byDirectionVec(Eigen::Vector3f Dir, Eigen::Vector3f Ref)
{
    Eigen::Vector3f X_axis = Ref.cross(Dir);
    X_axis.normalize();
    Eigen::Vector3f Y_axis = Dir.cross(X_axis);
    Y_axis.normalize();

    Mat3f ret;
    ret(0, 0) = X_axis(0);
    ret(0, 1) = Y_axis(0);
    ret(0, 2) = Dir(0);
    ret(1, 0) = X_axis(1);
    ret(1, 1) = Y_axis(1);
    ret(1, 2) = Dir(1);
    ret(2, 0) = X_axis(2);
    ret(2, 1) = Y_axis(2);
    ret(2, 2) = Dir(2);

    return ret;
}

Mat3f CalRotationMatrix_byDirectionVec(Eigen::Vector3f Dir, char axis1, Eigen::Vector3f Ref, char axis2)
{
    axis1 -= 'x';
    axis2 -= 'x';
    // char tmp[3] = {0, 0, 0};
    // tmp[axis1] = 1;
    // tmp[axis2] = 1;
    char axis3 = 3 - axis1 - axis2;
    // for(char i = 0; i < 3; i++)
    Eigen::Vector3f FirstAxis = Dir;  // axis1
    Eigen::Vector3f ThirdAxis = Ref.cross(Dir);  // The third axis are calculated by cross product between 
    Eigen::Vector3f SecondAxis = Dir.cross(ThirdAxis);  // axis2 reference from the [Ref] argument.

    FirstAxis.normalize();
    ThirdAxis.normalize();
    SecondAxis.normalize();

    // Eigen::Vector3f X_axis = Ref.cross(Dir);
    // X_axis.normalize();
    // Eigen::Vector3f Y_axis = Dir.cross(X_axis);
    // Y_axis.normalize();

    Mat3f ret;
    ret(0, axis1) = FirstAxis(0);
    ret(1, axis1) = FirstAxis(1);
    ret(2, axis1) = FirstAxis(2);
    ret(0, axis2) = SecondAxis(0);
    ret(1, axis2) = SecondAxis(1);
    ret(2, axis2) = SecondAxis(2);
    ret(0, axis3) = ThirdAxis(0);
    ret(1, axis3) = ThirdAxis(1);
    ret(2, axis3) = ThirdAxis(2);

    return ret;
}

inline PCXYZ_Ptr CopyPointCloud_MP(PCXYZ_Ptr source)
{
    PCXYZ_Ptr ret(new PCXYZ);
    ret->points.reserve(source->size());
    ret->points.resize(source->size());

    START_TIMER(copy1);
    tbb::parallel_for(tbb::blocked_range<size_t>(0, source->size()), [&](const tbb::blocked_range<size_t> &r) {
        for (size_t i = r.begin(); i != r.end(); ++i)
        {
            ret->points[i].x = source->points[i].x;
            ret->points[i].y = source->points[i].y;
            ret->points[i].z = source->points[i].z;
        }
    });
    STOP_TIMER(copy1);
    return ret;
}

#ifdef __TESTING_CODE__

class TBB_PC_Copyer
{
    PCXYZ_Ptr Source;

public:
    PCXYZ Out;
    void operator()(const tbb::blocked_range<size_t> &r)
    {
        PCXYZ_Ptr tmp_Source = Source;
        PCXYZ tmp_out;
        PointXYZ p;
        Out.points.resize(r.end() - r.begin());
        // tmp_out->points.resize(r.end() - r.begin());
        for (size_t i = r.begin(); i != r.end(); ++i)
        {

            Out.points[i - r.begin()].x = tmp_Source->points[i].x;
            Out.points[i - r.begin()].y = tmp_Source->points[i].y;
            Out.points[i - r.begin()].z = tmp_Source->points[i].z;
        }
        // Out = tmp_out;
    }

    TBB_PC_Copyer(TBB_PC_Copyer &x, tbb::split) : Source(x.Source), Out() {}

    void join(const TBB_PC_Copyer &y)
    {
        Out += (y.Out);
        __DEBUG_PROBE__(Out.points[0]);
    }

    TBB_PC_Copyer(PCXYZ_Ptr a) : Source(a), Out()
    {
        Out.reserve(Source->size());
    }
};

PCXYZ Copy_PC_MP(PCXYZ_Ptr Source, PCXYZ &out)
{
    TBB_PC_Copyer sf(Source);
    tbb::parallel_reduce(tbb::blocked_range<size_t>(0, Source->size(), Source->size() / 16), sf);

    out = sf.Out;
    return sf.Out;
}

inline PCXYZ_Ptr pcl_tbb_copy2(PCXYZ_Ptr source)
{
    const int THREADS = 16;
    PCXYZ_Ptr ret[THREADS];
    int size = source->size() / THREADS + 1;
    for (int i = 0; i != THREADS; i++)
    {
        ret[i] = PCXYZ_Ptr(new PCXYZ);
        ret[i]->points.reserve(size);
        ret[i]->points.resize(size);
    }

    START_TIMER(copy1);
    tbb::parallel_for(tbb::blocked_range<size_t>(0, THREADS, size / 16), [&](const tbb::blocked_range<size_t> &r) {
        for (size_t i = r.begin(); i != r.end(); ++i)
            for (size_t j = 0; j < size && i * size + j < source->size(); ++j)
            {
                ret[i]->points[j].x = source->points[i * size + j].x;
                ret[i]->points[j].y = source->points[i * size + j].y;
                ret[i]->points[j].z = source->points[i * size + j].z;
            }
    });
    STOP_TIMER(copy1);

    for (int i = 1; i != THREADS; i++)
    {
        *ret[0] += *ret[i];
    }
    return ret[0];
}

// This code is used to find a min and max point in the point cloud, but slower than now and cause wrong result.
template <char AXIS>
class TBB_PC_MaxMin_Finder
{
    PCXYZ_Ptr Source;

public:
    unsigned int min_idx = 0, max_idx = 0;
    void operator()(const tbb::blocked_range<size_t> &r)
    {
        PCXYZ_Ptr tmp_Source = Source;
        unsigned int tmp_min_idx = r.begin(), tmp_max_idx = r.begin();
        float tmp_min = -INFINITY, tmp_max = INFINITY;
        // PointXYZ tmp_min(-INFINITY, -INFINITY, -INFINITY), tmp_max(INFINITY, INFINITY, INFINITY);
        for (size_t i = r.begin(); i != r.end(); ++i)
        {
            if constexpr (AXIS == 'x')
            {
                if (tmp_Source->points[tmp_min_idx].x > tmp_Source->points[i].x)
                    tmp_min_idx = i;
                if (tmp_Source->points[tmp_max_idx].x < tmp_Source->points[i].x)
                    tmp_max_idx = i;
            }
            else if constexpr (AXIS == 'y')
            {
                if (tmp_Source->points[tmp_min_idx].y > tmp_Source->points[i].y)
                    tmp_min_idx = i;
                if (tmp_Source->points[tmp_max_idx].y < tmp_Source->points[i].y)
                    tmp_max_idx = i;
            }
            else if constexpr (AXIS == 'z')
            {
                if (tmp_Source->points[tmp_min_idx].z > tmp_Source->points[i].z)
                    tmp_min_idx = i;
                if (tmp_Source->points[tmp_max_idx].z < tmp_Source->points[i].z)
                    tmp_max_idx = i;
            }
        }
        min_idx = tmp_min_idx;
        max_idx = tmp_max_idx;
    }

    TBB_PC_MaxMin_Finder(TBB_PC_MaxMin_Finder &x, tbb::split) : Source(x.Source) {}

    void join(const TBB_PC_MaxMin_Finder &_sub)
    {
        // center = y.center + center;
        if constexpr (AXIS == 'x')
        {
            if (Source->points[min_idx].x > _sub.Source->points[_sub.min_idx].x)
                min_idx = _sub.min_idx;
            if (Source->points[max_idx].x < _sub.Source->points[_sub.max_idx].x)
                max_idx = _sub.max_idx;
        }
        else if constexpr (AXIS == 'y')
        {
            if (Source->points[min_idx].y > _sub.Source->points[_sub.min_idx].y)
                min_idx = _sub.min_idx;
            if (Source->points[max_idx].y < _sub.Source->points[_sub.max_idx].y)
                max_idx = _sub.max_idx;
        }
        else if constexpr (AXIS == 'z')
        {
            if (Source->points[min_idx].z > _sub.Source->points[_sub.min_idx].z)
                min_idx = _sub.min_idx;
            if (Source->points[max_idx].z < _sub.Source->points[_sub.max_idx].z)
                max_idx = _sub.max_idx;
        }
    }

    TBB_PC_MaxMin_Finder(PCXYZ_Ptr a) : Source(a)
    {
    }
};

void Cal_Normal_CUDA(PCXYZ_Ptr Source, PCXYZN_Ptr &out, double radius)
{
    pcl::gpu::NormalEstimation::PointCloud cloud;
    cloud.upload(Source->points);

    pcl::gpu::NormalEstimation ne;
    ne.setInputCloud(cloud);

    pcl::gpu::Feature::Normals normals_small;
    ne.setRadiusSearch(0.05, 100); // They didn't explain meaning of the second parameter.
    ne.compute(normals_small);

    PointXYZ normals_small_host[Source->points.size()];
    normals_small.download(&normals_small_host[0]);

    out = PCXYZN_Ptr(new PCXYZN);
    out->width = Source->points.size();
    out->height = 1;
    for (int x = 0; x < Source->points.size(); x++)
    {
        pcl::PointNormal np;

        pcl::Normal *n = (pcl::Normal *)&normals_small_host[x];
        PointXYZ p = Source->points[x];

        np.x = p.x;
        np.y = p.y;
        np.z = p.z;
        np.normal_x = n->normal_x;
        np.normal_y = n->normal_y;
        np.normal_z = n->normal_z;
        np.curvature = n->curvature;
        out->points.push_back(np);
    }
}

#endif

} // namespace CookTeam
