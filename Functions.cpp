/**
 * @file Functions.cpp
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2019-06-05
 * 
 * @mainpage
 * Before you using this library
 * Arranged by frequency of using.
 * # Functions Introduction
 * ## Filter
 * #MakeTransformMatrix (double)
 * ## Feature Extractor
 * ## PCA
 * ## Neighbor Finding
 * ## ICP
 * ## Point Cloud Operation
 * #MakeTransformMatrix("asdasd")
 * 
 * @copyright Copyright (c) 2019
 * 
 */

#include "Functions.h"
#include <queue>
#include <mutex>












/**
 * @brief (I forgot the function of this function)Calculate <a href="http://docs.pointclouds.org/trunk/classpcl_1_1_s_h_o_t_estimation.html">SHOT</a> features in the model and scene.
 * The performance is not good, DO NOT use this function.
 * 
 * @param Model 
 * @param Model_Normal 
 * @param Scene 
 * @param Scene_Normal 
 * @param RadiusSearch 
 * @return pcl::CorrespondencesPtr 
 */
pcl::CorrespondencesPtr FindCorres_WithSHOTFeature(PCXYZ_Ptr Model, PC_Normal_Ptr Model_Normal, PCXYZ_Ptr Scene, PC_Normal_Ptr Scene_Normal, float RadiusSearch)
{
	pcl::PointCloud<DescriptorType>::Ptr model_descriptors (new pcl::PointCloud<DescriptorType> ());
	pcl::PointCloud<DescriptorType>::Ptr scene_descriptors (new pcl::PointCloud<DescriptorType> ());
	//
	//  Compute Descriptor for keypoints
	//
	pcl::SHOTEstimationOMP<PointXYZ, pcl::Normal, DescriptorType> descr_est;
	descr_est.setRadiusSearch (RadiusSearch);

	descr_est.setInputCloud (Model);
	descr_est.setInputNormals (Model_Normal);
	descr_est.setSearchSurface (Model);
	descr_est.compute (*model_descriptors);

	descr_est.setInputCloud (Scene);
	descr_est.setInputNormals (Scene_Normal);
	descr_est.setSearchSurface (Scene);
	descr_est.compute (*scene_descriptors);

	//
	//  Find Model-Scene Correspondences with KdTree
	//
	pcl::CorrespondencesPtr model_scene_corrs (new pcl::Correspondences ());

	pcl::KdTreeFLANN<DescriptorType> match_search;
	match_search.setInputCloud (model_descriptors);

	//  For each scene keypoint descriptor, find nearest neighbor into the model keypoints descriptor cloud and add it to the correspondences vector.
	for (size_t i = 0; i < scene_descriptors->size (); ++i)
	{
		std::vector<int> neigh_indices (1);
		std::vector<float> neigh_sqr_dists (1);
		if (!__finite (scene_descriptors->at (i).descriptor[0])) //skipping NaNs
			continue;
		int found_neighs = match_search.nearestKSearch (scene_descriptors->at (i), 1, neigh_indices, neigh_sqr_dists);
		if(found_neighs == 1 && neigh_sqr_dists[0] < 0.25f) //  add match only if the squared descriptor distance is less than 0.25 (SHOT descriptor distances are between 0 and 1 by design)
		{
			pcl::Correspondence corr (neigh_indices[0], static_cast<int> (i), neigh_sqr_dists[0]);
			model_scene_corrs->push_back (corr);
		}
	}
	std::cout << "Correspondences found: " << model_scene_corrs->size () << std::endl;
	return model_scene_corrs;
}


/**
 * @brief Matching model in the scene using SHOT feature.
 * The performance is not good, DO NOT use this function.
 * 
 * @param Model : This point cloud must be downsampled.
 * @param Scene : This point cloud must be downsampled.
 * @return : Number of recognized PointCloud.
 */
int RecognitionModel(PCXYZ_Ptr Model, PCXYZ_Ptr Scene, stdVector_Mat4f &Tran, std::vector<pcl::Correspondences> &clustered_corrs, RegnSetting Setting)
{
	pcl::PointCloud<pcl::ReferenceFrame>::Ptr Model_RefFrame (new pcl::PointCloud<pcl::ReferenceFrame> ());
	pcl::PointCloud<pcl::ReferenceFrame>::Ptr Scene_RefFrame (new pcl::PointCloud<pcl::ReferenceFrame> ());

	PC_Normal_Ptr Model_Normal(new PC_Normal);
	PC_Normal_Ptr Scene_Normal(new PC_Normal);

	pcl::BOARDLocalReferenceFrameEstimation<PointXYZ, pcl::Normal, pcl::ReferenceFrame> rf_est;
	pcl::Hough3DGrouping<PointXYZ, PointXYZ, pcl::ReferenceFrame, pcl::ReferenceFrame> clusterer;

	pcl::NormalEstimationOMP<PointXYZ, pcl::Normal> norm_est;
	norm_est.setKSearch (Setting.NormalKSearch);
	norm_est.setInputCloud (Model);
	norm_est.compute (*Model_Normal);
	norm_est.setInputCloud (Scene);
	norm_est.compute (*Scene_Normal);

	// Compute Model-Scene Correspondences.
	pcl::CorrespondencesPtr model_scene_corrs = FindCorres_WithSHOTFeature(Model, Model_Normal, Scene, Scene_Normal, 0.01f);

	// Compute (Keypoints) Reference Frames for Hough
	rf_est.setFindHoles (Setting.BOARD_RF_FindHole);
	rf_est.setRadiusSearch (Setting.BOARD_RF_RadiusSearch);

	rf_est.setInputCloud (Model);
	rf_est.setInputNormals (Model_Normal);
	rf_est.setSearchSurface (Model);
	rf_est.compute (*Model_RefFrame);

	rf_est.setInputCloud (Scene);
	rf_est.setInputNormals (Scene_Normal);
	rf_est.setSearchSurface (Scene);
	rf_est.compute (*Scene_RefFrame);

	// Clustering
	clusterer.setHoughBinSize (Setting.HoughBinSize);
	clusterer.setHoughThreshold (Setting.HoughThreshold);
	clusterer.setUseInterpolation (Setting.HoughUseInterpolation);
	clusterer.setUseDistanceWeight (Setting.HoughUseDistanceWeight);

	clusterer.setInputCloud (Model);
	clusterer.setInputRf (Model_RefFrame);
	clusterer.setSceneCloud (Scene);
	clusterer.setSceneRf (Scene_RefFrame);
	clusterer.setModelSceneCorrespondences (model_scene_corrs);

	clusterer.recognize (Tran, clustered_corrs);

	std::cout << "RecognitionModel(): Model instances found: " << Tran.size () << std::endl;
	for (size_t i = 0; i < Tran.size (); ++i)
	{
		std::cout << "\nRecognitionModel(): Instance " << i + 1 << ":" << std::endl;
		std::cout << "        Correspondences belonging to this instance: " << clustered_corrs[i].size () << std::endl;

		// Print the rotation matrix and translation vector
		Eigen::Matrix3f rotation = Tran[i].block<3,3>(0, 0);
		Eigen::Vector3f translation = Tran[i].block<3,1>(0, 3);

		printf ("\n");
		printf ("            | %6.3f %6.3f %6.3f | \n", rotation (0,0), rotation (0,1), rotation (0,2));
		printf ("        R = | %6.3f %6.3f %6.3f | \n", rotation (1,0), rotation (1,1), rotation (1,2));
		printf ("            | %6.3f %6.3f %6.3f | \n", rotation (2,0), rotation (2,1), rotation (2,2));
		printf ("\n");
		printf ("        t = < %0.3f, %0.3f, %0.3f >\n", translation (0), translation (1), translation (2));
	}
	return Tran.size();
}


/**
 * @brief Calculate the third axis direction using cross product. This function intend to make a coordinate axis vector.
 * 
 * @param LeftVec Left vector in cross product.
 * @param RightVec Right vector in cross product
 * @param isRightHandCoordinate If false the LEFT hand coordinate will be calculated as result and vice versa.
 * @return Vec3f Result vector with Eigen::Vec3f type.
 */
inline Vec3f Cal_ThirdAxisDir(Vec3f LeftVec, Vec3f RightVec, bool isRightHandCoordinate)
{
	if(isRightHandCoordinate)
		return LeftVec.cross(RightVec);
	else
		return -LeftVec.cross(RightVec);
}


PlyMesh_Ptr CreatePlyMesh(PCXYZ_Ptr Data)
{
	PlyMesh_Ptr Mesh(new PlyMesh);

	// Normal estimation*
	pcl::NormalEstimationOMP<pcl::PointXYZ, pcl::Normal> n;
	pcl::PointCloud<pcl::Normal>::Ptr normals (new pcl::PointCloud<pcl::Normal>);
	pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
	tree->setInputCloud (Data);
	n.setInputCloud (Data);
	n.setSearchMethod (tree);
	n.setKSearch (20);
	n.compute (*normals);
	//* normals should not contain the point normals + surface curvatures

	// Concatenate the XYZ and normal fields*
	pcl::PointCloud<pcl::PointNormal>::Ptr cloud_with_normals (new pcl::PointCloud<pcl::PointNormal>);
	pcl::concatenateFields (*Data, *normals, *cloud_with_normals);
	//* cloud_with_normals = cloud + normals

	// Create search tree*
	pcl::search::KdTree<pcl::PointNormal>::Ptr tree2 (new pcl::search::KdTree<pcl::PointNormal>);
	tree2->setInputCloud (cloud_with_normals);

	// Initialize objects
	pcl::GreedyProjectionTriangulation<pcl::PointNormal> gp3;
	PlyMesh triangles;

	// Set the maximum distance between connected points (maximum edge length)
	gp3.setSearchRadius (0.025);

	// Set typical values for the parameters
	gp3.setMu (2.5);
	gp3.setMaximumNearestNeighbors (100);
	gp3.setMaximumSurfaceAngle(M_PI/4); // 45 degrees
	gp3.setMinimumAngle(M_PI/18); // 10 degrees
	gp3.setMaximumAngle(2*M_PI/3); // 120 degrees
	gp3.setNormalConsistency(false);

	// Get result
	gp3.setInputCloud (cloud_with_normals);
	gp3.setSearchMethod (tree2);
	gp3.reconstruct (*Mesh);

	return Mesh;
}




Vec6f Cvt_TransformMatrix2xyzabc(Mat4f Data, int RotationOrd1, int RotationOrd2, int RotationOrd3)
{
	Mat3f RotationMat = Data.block<3,3>(0,0);
	Vec3f EulerAngle = RotationMat.eulerAngles(RotationOrd1, RotationOrd2, RotationOrd3);
	Vec3f TranslateVector = Data.block<3,1>(0,3);

	Vec6f Ret;
	Ret(0) = TranslateVector[0];
	Ret(1) = TranslateVector[1];
	Ret(2) = TranslateVector[2];
	Ret(3) = EulerAngle[0];
	Ret(4) = EulerAngle[1];
	Ret(5) = EulerAngle[2];

	return Ret;
}


PCXYZ_Ptr Cvt_PCXYZN_To_PCXYZ(PCXYZN_Ptr Source)
{
	PCXYZ_Ptr out(new PCXYZ);
	PointXYZ tmp;
	for(PointXYZN point : Source->points)
	{
		tmp.x = point.x;
		tmp.y = point.y;
		tmp.z = point.z;
		out->points.push_back(tmp);
	}
	return out;
}



#ifdef __NEED_READ_FROM_FILE__
/// dddd
bool ReadPointCloudData(std::string Path, PCXYZ_Ptr Output)
{
	PCXYZ_Ptr tmp4(new PCXYZ);
	PCXYZ_Ptr tmp5(new PCXYZ);
	pcl::VoxelGrid<pcl::PointXYZ> sor;//新声明体素网格对象
	//sor.setLeafSize(0.001414f, 0.001414f, 0.001414f);//设定体素网格大小
	sor.setLeafSize(0.005f, 0.005f, 0.005f);//设定体素网格大小
	std::mutex ReadingMutex;
	#pragma omp parallel for firstprivate(Output),lastprivate(Output)
	for (int i = 0; i < NUM_OF_DATA; i++)
	{
		PCXYZ_Ptr tmp(new PCXYZ);
		PCXYZ_Ptr tmp2(new PCXYZ);
		PCXYZ_Ptr tmp3(new PCXYZ);
		////D:\\采集数据\\2017-11-23_13-59-40 普通放置   C:\\Users\\Dong\\Documents\\Visual Studio 2017\\Projects\\PCL-重新校准\\x64\\Debug\\tmp
		//("D:\\采集数据\\2017-11-23_14-06-26 带有倾斜\\PointCloud_"
		if (pcl::io::loadPCDFile(Path + std::to_string(i) + ".pcd",	*tmp) < 0)
		{
			std::cout << "Can't load PointCloud_" + std::to_string(i) + ".pcd" << std::endl;
		}
		else
		{
			std::cout << "Load PointCloud_" + std::to_string(i) + ".pcd to Res." << std::endl;
			pcl::transformPointCloud(*tmp, *tmp2, MakeTransformMatrix(TransData[i]));
			Filters(tmp2, tmp3);

			while(!ReadingMutex.try_lock());
			*Output += *tmp3;
			ReadingMutex.unlock();
		}
	}
	tmp4 = Output;
	sor.setInputCloud(tmp4);
	sor.filter(*Output);//过滤点云
	return true;
}
#endif
