#include "type_convention.hpp"

namespace CookTeam
{

Vec6f Cvt_TransformMatrix2xyzabc(Mat4f Data, int RotationOrd1, int RotationOrd2, int RotationOrd3)
{
    Mat3f RotationMat = Data.block<3, 3>(0, 0);
    Vec3f EulerAngle = RotationMat.eulerAngles(RotationOrd1, RotationOrd2, RotationOrd3);
    Vec3f TranslateVector = Data.block<3, 1>(0, 3);

    Vec6f Ret;
    Ret(0) = TranslateVector[0];
    Ret(1) = TranslateVector[1];
    Ret(2) = TranslateVector[2];
    Ret(3) = EulerAngle[0];
    Ret(4) = EulerAngle[1];
    Ret(5) = EulerAngle[2];

    return Ret;
}

PCXYZ_Ptr Cvt_PCXYZN_To_PCXYZ(PCXYZN_Ptr Source)
{
    PCXYZ_Ptr out(new PCXYZ);
    PointXYZ tmp;

    for (PointXYZN point : Source->points)
    {
        tmp.x = point.x;
        tmp.y = point.y;
        tmp.z = point.z;
        out->points.push_back(tmp);
    }
    return out;
}
} // namespace CookTeam
