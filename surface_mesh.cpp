#include "surface_mesh.hpp"


namespace CookTeam
{
	/**************************************
	 * @brief: Smooth the surface with the given point cloud.
	 * @param[in]:	PCXYZ_Ptr Source : A pointer to point cloud that you want to smooth.
	 * @param[in]:	float SearchRadius : The higher you set, the surface is more flatter.
	 * @return:		PCXYZ_Ptr : Smoothed point cloud pointer.
	 * @note: 
	 * @author: Dong
	 * @date: 2019/06/04-20:10:03
	 **************************************/
	PCXYZ_Ptr ReconstructSurface(PCXYZ_Ptr Source, float SearchRadius)
	{
		if((*Source).size() == 0)
			return PCXYZ_Ptr(new PCXYZ);

		// Create a KD-Tree
		pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);

		// Init reconstruction object (second point type is for the normals, even if unused)
		pcl::MovingLeastSquares<pcl::PointXYZ, pcl::PointXYZ> mls;

		// Set parameters
		mls.setInputCloud (Source);
		mls.setPolynomialOrder (true);
		mls.setSearchMethod (tree);
		mls.setSearchRadius (SearchRadius);

		// Reconstruct
		PCXYZ_Ptr output(new PCXYZ);
		mls.process(*output);

		return output;
	}


	/**************************************
	 * @brief: (NOT WORKED!!!)Test function. Smooth the surface and fill the hole with the given point cloud.
	 * @param[in]:	PCXYZ_Ptr Source : A pointer to point cloud that you want to smooth.
	 * @param[in]:	float SearchRadius : The higher you set, the surface is more flatter.
	 * @return:		PCXYZ_Ptr : Smoothed point cloud pointer.
	 * @note:
	 * @author: Dong
	 * @date: 2019/06/04-20:27:43
	 **************************************/
	PCXYZ_Ptr ReconstructSurface_FillHole(PCXYZ_Ptr Source, float SearchRadius)
	{
		if((*Source).size() == 0)
			return PCXYZ_Ptr(new PCXYZ);

		// Create a KD-Tree
		pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);

		// Init reconstruction object (second point type is for the normals, even if unused)
	//	pcl::MovingLeastSquares<pcl::PointXYZ, pcl::PointXYZ> mls;
		pcl::MovingLeastSquares<pcl::PointXYZ, pcl::PointNormal> mls;

		mls.setUpsamplingMethod (pcl::MovingLeastSquares<pcl::PointXYZ, pcl::PointNormal>::SAMPLE_LOCAL_PLANE);
		mls.setUpsamplingRadius (0.002);
		mls.setUpsamplingStepSize (0.001);
	//	mls.setUpsamplingMethod (pcl::MovingLeastSquares<pcl::PointXYZ, pcl::PointNormal>::VOXEL_GRID_DILATION);
	//	mls.setDilationIterations(5);
	//	mls.setDilationVoxelSize(0.00005);
		mls.setPolynomialOrder(2);
		mls.setComputeNormals (true);

		// Set parameters
		mls.setInputCloud (Source);
	//	mls.setPolynomialOrder (true);
		mls.setSearchMethod (tree);
		mls.setSearchRadius (SearchRadius);

		// Reconstruct
		PCXYZN_Ptr out(new PCXYZN);
		mls.process(*out);
		PCXYZ_Ptr output(new PCXYZ);
		output = Cvt_PCXYZN_To_PCXYZ(out);

		return output;
	}


	PlyMesh_Ptr CreatePlyMesh(PCXYZ_Ptr Data)
	{
		PlyMesh_Ptr Mesh(new PlyMesh);

		// Normal estimation*
		pcl::NormalEstimationOMP<pcl::PointXYZ, pcl::Normal> n;
		pcl::PointCloud<pcl::Normal>::Ptr normals (new pcl::PointCloud<pcl::Normal>);
		pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
		tree->setInputCloud (Data);
		n.setInputCloud (Data);
		n.setSearchMethod (tree);
		n.setKSearch (20);
		n.compute (*normals);
		//* normals should not contain the point normals + surface curvatures

		// Concatenate the XYZ and normal fields*
		pcl::PointCloud<pcl::PointNormal>::Ptr cloud_with_normals (new pcl::PointCloud<pcl::PointNormal>);
		pcl::concatenateFields (*Data, *normals, *cloud_with_normals);
		//* cloud_with_normals = cloud + normals

		// Create search tree*
		pcl::search::KdTree<pcl::PointNormal>::Ptr tree2 (new pcl::search::KdTree<pcl::PointNormal>);
		tree2->setInputCloud (cloud_with_normals);

		// Initialize objects
		pcl::GreedyProjectionTriangulation<pcl::PointNormal> gp3;
		PlyMesh triangles;

		// Set the maximum distance between connected points (maximum edge length)
		gp3.setSearchRadius (0.025);

		// Set typical values for the parameters
		gp3.setMu (2.5);
		gp3.setMaximumNearestNeighbors (100);
		gp3.setMaximumSurfaceAngle(M_PI/4); // 45 degrees
		gp3.setMinimumAngle(M_PI/18); // 10 degrees
		gp3.setMaximumAngle(2*M_PI/3); // 120 degrees
		gp3.setNormalConsistency(false);

		// Get result
		gp3.setInputCloud (cloud_with_normals);
		gp3.setSearchMethod (tree2);
		gp3.reconstruct (*Mesh);

		return Mesh;
	}
}
