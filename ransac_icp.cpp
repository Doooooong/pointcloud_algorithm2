#include "ransac_icp.hpp"

namespace CookTeam
{
	/* ********************************************
	*                   RANSAC
	* *******************************************/
	// ExtractPlane: Points limit version
	/**
	 * @brief Extract the plane with max number of points. The point number of extracted plane must larger than [MinPoints]
	 * 
	 * @param Source Oringin point cloud pointer.
	 * @param Plane The plane extracted.
	 * @param Rest The rest of points.
	 * @param ThreadHold The thinkness of plane.
	 * @param MinPoints The point number of extracted plane must be larger than [MinPoints]. If not, return empty or zeros.
	 * @return Vector6f [Center point x, Center point y, Center point z, Normal x, Normal y, Normal z]
	 */
	Vector6f ExtractPlane(PCXYZ_Ptr Source, PCXYZ_Ptr Plane, PCXYZ_Ptr Rest, float ThreadHold, int MinPoints)//抽出平面，Rest去除平面后的点云；返回值是抽出的平面。
	{
		pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
		pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
		// Create the segmentation object
		pcl::SACSegmentation<pcl::PointXYZ> seg;
		// Optional
		seg.setOptimizeCoefficients(true);
		// Mandatory
		seg.setModelType(pcl::SACMODEL_PLANE);
		seg.setMethodType(pcl::SAC_RANSAC);
		seg.setDistanceThreshold(ThreadHold);//0.02

		seg.setInputCloud(Source);
		seg.segment(*inliers, *coefficients);

		if (inliers->indices.size() <= (unsigned int)MinPoints)// 控制最小点数
		{
			PCL_ERROR("ExtractPlane(): Could not estimate a planar model for the given dataset.");
			Plane->clear();
			Rest->clear();
			return Vector6f().setZero();
		}

		//输出处理
		pcl::ExtractIndices<pcl::PointXYZ> extract;//由于SACSegmentation抽出来的只是点的数值，需要将数值转化为点云。这个类便是专门来干这个的。
		extract.setInputCloud(Source);
		extract.setIndices(inliers);
		extract.setNegative(false);
		// Get the points associated with the planar surface
		extract.filter(*Plane);
		// Remove the planar inliers, extract the rest
		extract.setNegative(true);
		extract.filter(*Rest);

		PCAOut Res = Cal_PCA(Plane);
		Vector6f Ret;
		Ret(0) = Res.Transform[0];  // Center point x
		Ret(1) = Res.Transform[1];  // Center point y
		Ret(2) = Res.Transform[2];  // Center point z
		Ret(3) = coefficients->values[0];  // Direction x
		Ret(4) = coefficients->values[1];  // Direction y
		Ret(5) = coefficients->values[2];  // Direction z

		return Ret;
	}
	// ExtractPlane: Percent limit version
	/**
	 * @brief Extract the plane with max number of points.
	 * 
	 * @param Source Oringin point cloud pointer.
	 * @param Plane The plane extracted.
	 * @param Rest The rest of points.
	 * @param ThreadHold The thinkness of plane.
	 * @param Percent The point number of extracted plane must be larger than [point number of Source]
	 * @return Vector6f [Center point x, Center point y, Center point z, Normal x, Normal y, Normal z]
	 */
	Vector6f ExtractPlane(PCXYZ_Ptr Source, PCXYZ_Ptr Plane, PCXYZ_Ptr Rest, float ThreadHold, double Percent)//抽出平面，Rest去除平面后的点云；返回值是抽出的平面。
	{
		pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
		pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
		// Create the segmentation object
		pcl::SACSegmentation<pcl::PointXYZ> seg;
		// Optional
		seg.setOptimizeCoefficients(true);
		// Mandatory
		seg.setModelType(pcl::SACMODEL_PLANE);
		seg.setMethodType(pcl::SAC_RANSAC);
		seg.setDistanceThreshold(ThreadHold);//0.02

		seg.setInputCloud(Source);
		seg.segment(*inliers, *coefficients);
		if (inliers->indices.size() <= (*Source).size() * Percent)// 控制最小点数
		{
			PCL_ERROR("ExtractPlane(): Could not estimate a planar model for the given dataset.");
			Plane->clear();
			Rest->clear();
			return Vector6f().setZero();
		}

		//输出处理
		pcl::ExtractIndices<pcl::PointXYZ> extract;//由于SACSegmentation抽出来的只是点的数值，需要将数值转化为点云。这个类便是专门来干这个的。
		extract.setInputCloud(Source);
		extract.setIndices(inliers);
		extract.setNegative(false);
		// Get the points associated with the planar surface
		extract.filter(*Plane);
		// Remove the planar inliers, extract the rest
		extract.setNegative(true);
		extract.filter(*Rest);

		PCAOut Res = Cal_PCA(Plane);
		Vector6f Ret;
		Ret(0) = Res.Transform[0];  // Center point x
		Ret(1) = Res.Transform[1];  // Center point y
		Ret(2) = Res.Transform[2];  // Center point z
		Ret(3) = coefficients->values[0];  // Direction x
		Ret(4) = coefficients->values[1];  // Direction y
		Ret(5) = coefficients->values[2];  // Direction z

		return Ret;
	}

	// ExtractPlane: Points limit version
	/**
	 * @brief (NOT WORKED!!!)Extract the plane with same normal specified by [Axis].
	 * 
	 * @param Source Oringin point cloud pointer.
	 * @param Axis The direction of plane that you want to extract.
	 * @param Plane The plane extracted.
	 * @param Rest The rest of points.
	 * @param ThreadHold The thinkness of plane.
	 * @param MinPoints The point number of extracted plane must be larger than [MinPoints]. If not, return empty or zeros.
	 * @return Vector6f [Center point x, Center point y, Center point z, Normal x, Normal y, Normal z]
	 */
	Vector6f ExtractParallelPlane(PCXYZ_Ptr Source, Vec3f Axis, PCXYZ_Ptr Plane, PCXYZ_Ptr Rest, float ThreadHold, int MinPoints)//抽出平面，Rest去除平面后的点云；返回值是抽出的平面。
	{
		pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
		pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
		// Create the segmentation object
		pcl::SACSegmentation<pcl::PointXYZ> seg;
		// Optional
		seg.setOptimizeCoefficients(true);
		// Mandatory
		seg.setAxis(Axis);
		seg.setOptimizeCoefficients(false);
		seg.setEpsAngle(  10.0f * TO_RAD);
		seg.setModelType(pcl::SACMODEL_PERPENDICULAR_PLANE);
		seg.setMethodType(pcl::SAC_RANSAC);
		seg.setDistanceThreshold(ThreadHold);//0.02

		seg.setInputCloud(Source);
		seg.segment(*inliers, *coefficients);

		if (inliers->indices.size() <= (unsigned int)MinPoints)// 控制最小点数
		{
			PCL_ERROR("ExtractPlane(): Could not estimate a planar model for the given dataset.");
			Plane->clear();
			Rest->clear();
			return Vector6f().setZero();
		}

		//输出处理
		pcl::ExtractIndices<pcl::PointXYZ> extract;//由于SACSegmentation抽出来的只是点的数值，需要将数值转化为点云。这个类便是专门来干这个的。
		extract.setInputCloud(Source);
		extract.setIndices(inliers);
		extract.setNegative(false);
		// Get the points associated with the planar surface
		extract.filter(*Plane);
		// Remove the planar inliers, extract the rest
		extract.setNegative(true);
		extract.filter(*Rest);

		PCAOut Res = Cal_PCA(Plane);
		Vector6f Ret;
		Ret(0) = Res.Transform[0];  // Center point x
		Ret(1) = Res.Transform[1];  // Center point y
		Ret(2) = Res.Transform[2];  // Center point z
		Ret(3) = coefficients->values[0];  // Direction x
		Ret(4) = coefficients->values[1];  // Direction y
		Ret(5) = coefficients->values[2];  // Direction z

		return Ret;
	}

	/**
	 * @brief Extract the cylinder shape using RANSAC.
	 * 
	 * @param Source Oringin point cloud pointer.
	 * @param Cylinder The cylinder extracted.
	 * @param Rest The rest of points.
	 * @param ThreadHold The thinkness of cylinder.
	 * @param MinPoints The point number of extracted cylinder must be larger than [MinPoints]. If not, return empty or zeros.
	 * @return Vector7f [Center point x, Center point y, Center point z, Direction x, Direction y, Direction z, Radius].
	 */
	Vector7f ExtractCylinder(PCXYZ_Ptr Source, PCXYZ_Ptr Cylinder, PCXYZ_Ptr Rest, float ThreadHold, double Percent)
	{
		pcl::search::KdTree<PointXYZ>::Ptr tree (new pcl::search::KdTree<PointXYZ> ());
		pcl::NormalEstimationOMP<PointXYZ, pcl::Normal> ne;
		pcl::PointCloud<pcl::Normal>::Ptr Normals (new pcl::PointCloud<pcl::Normal>);
		pcl::SACSegmentationFromNormals<PointXYZ, pcl::Normal> seg;

		ne.setSearchMethod (tree);
		ne.setInputCloud (Source);
		ne.setKSearch (15);
		ne.compute (*Normals);

		pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
		pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
		// Optional
		seg.setOptimizeCoefficients(true);
		// Mandatory
		seg.setModelType (pcl::SACMODEL_CYLINDER);
		seg.setMethodType (pcl::SAC_MLESAC);
		seg.setNormalDistanceWeight (0.1);
		seg.setMaxIterations (10000);
		seg.setDistanceThreshold (ThreadHold);
		seg.setRadiusLimits (0, 0.1);
		seg.setInputCloud (Source);
		seg.setInputNormals (Normals);

		// Obtain the cylinder inliers and coefficients
		seg.segment (*inliers, *coefficients);
		std::cerr << "Cylinder coefficients: " << *coefficients << std::endl;

		if (inliers->indices.size() <= (*Source).size() * Percent)  // 控制最小点数
		{
			PCL_ERROR("Could not estimate a cylinder model for the given dataset.");
			return Vector7f().setZero();
		}

		//输出处理
		pcl::ExtractIndices<pcl::PointXYZ> extract;//由于SACSegmentation抽出来的只是点的数值，需要将数值转化为点云。这个类便是专门来干这个的。
		extract.setInputCloud(Source);
		extract.setIndices(inliers);
		extract.setNegative(false);
		// Get the points associated with the planar surface
		extract.filter(*Cylinder);
		// Remove the planar inliers, extract the rest
		extract.setNegative(true);
		extract.filter(*Rest);
		Vector7f Ret;
		Ret(0) = coefficients->values[0];  // Center point x
		Ret(1) = coefficients->values[1];  // Center point y
		Ret(2) = coefficients->values[2];  // Center point z
		Ret(3) = coefficients->values[3];  // Direction x
		Ret(4) = coefficients->values[4];  // Direction y
		Ret(5) = coefficients->values[5];  // Direction z
		Ret(6) = coefficients->values[6];  // Radius
		return Ret;
	}


	/**
	 * @brief 
	 * 
	 * @param Source Oringin point cloud pointer.
	 * @param Circle The circle extracted.
	 * @param Rest The rest of points.
	 * @param ThreadHold The thinkness of circle.
	 * @param MinPoints The point number of extracted circle must be larger than [MinPoints]. If not, return empty or zeros.
	 * @return Vector7f [Center point x, Center point y, Center point z, Direction x, Direction y, Direction z, Radius].
	 */
	Vector7f ExtractCircle3D(PCXYZ_Ptr Source, PCXYZ_Ptr Circle, PCXYZ_Ptr Rest, float ThreadHold, double Percent)
	{
		pcl::search::KdTree<PointXYZ>::Ptr tree (new pcl::search::KdTree<PointXYZ> ());
		pcl::NormalEstimationOMP<PointXYZ, pcl::Normal> ne;
		pcl::PointCloud<pcl::Normal>::Ptr Normals (new pcl::PointCloud<pcl::Normal>);
		pcl::SACSegmentationFromNormals<PointXYZ, pcl::Normal> seg;

		ne.setSearchMethod (tree);
		ne.setInputCloud (Source);
		ne.setKSearch (50);
		ne.compute (*Normals);

		pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
		pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
		// Optional
		seg.setOptimizeCoefficients(true);
		// Mandatory
		seg.setModelType (pcl::SACMODEL_CIRCLE3D);
		seg.setMethodType (pcl::SAC_RANSAC);
		seg.setNormalDistanceWeight (0.1);
		seg.setMaxIterations (10000);
		seg.setDistanceThreshold (ThreadHold);
		seg.setRadiusLimits (0, 0.1);
		seg.setInputCloud (Source);
		seg.setInputNormals (Normals);

		// Obtain the cylinder inliers and coefficients
		seg.segment (*inliers, *coefficients);
		std::cerr << "Cylinder coefficients: " << *coefficients << std::endl;

		if (inliers->indices.size() <= (*Source).size() * Percent)  // 控制最小点数
		{
			PCL_ERROR("Could not estimate a cylinder model for the given dataset.");
			return Vector7f().setZero();
		}

		//输出处理
		pcl::ExtractIndices<pcl::PointXYZ> extract;//由于SACSegmentation抽出来的只是点的数值，需要将数值转化为点云。这个类便是专门来干这个的。
		extract.setInputCloud(Source);
		extract.setIndices(inliers);
		extract.setNegative(false);
		// Get the points associated with the planar surface
		extract.filter(*Circle);
		// Remove the planar inliers, extract the rest
		extract.setNegative(true);
		extract.filter(*Rest);

		Vector7f Ret;
		Ret(0) = coefficients->values[0];  // Center point x
		Ret(1) = coefficients->values[1];  // Center point y
		Ret(2) = coefficients->values[2];  // Center point z
		Ret(3) = coefficients->values[3];  // Radius
		Ret(4) = coefficients->values[4];  // Direction x
		Ret(5) = coefficients->values[5];  // Direction y
		Ret(6) = coefficients->values[6];  // Direction z

		return Ret;
	}


	/* ********************************************
	*                    ICP
	* *******************************************/
	/**
	 * @brief ICP function
	 * Find [the pose and position of model] in the scene.
	 * ICP limited version
	 * If angle or transformation over the limit, this function will not transform input pointcloud.
	 * AngleLimit is in Degrees. TranLimit is in meter.
	 * @param Source The point cloud will be moved. (Always input the model.)
	 * @param Target The point cloud not be moved. (Always input the scene.)
	 * @param Output Moved [Source] point cloud.
	 * @param Setting Settings of ICP algorithm.
	 * @return Mat4f The pose and position of [Source] in [Target] point cloud coordinate.
	 */
	Mat4f ICP_Single_Limit(PCXYZ_Ptr Source, PCXYZ_Ptr Target, PCXYZ_Ptr &Output, ICP_Setting Setting)
	{
		pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;

		icp.setMaxCorrespondenceDistance(Setting.MaxCorrespondenceDistance);
		icp.setTransformationEpsilon(Setting.TransformationEpsilon);
		icp.setEuclideanFitnessEpsilon(Setting.EuclideanFitnessEpsilon);

		icp.setMaximumIterations(Setting.MaximumIterations);
		icp.setInputSource((*Source).makeShared());
		icp.setInputTarget(Target);//这个点云不动，其他点云跟Target配合。
		icp.align(*Output);

		if (icp.hasConverged())
		{
			std::cout << "ICP has converged, score is " << icp.getFitnessScore() << ".";

			if (icp.getFitnessScore() > Setting.ScoreLimit)
			{
				PCL_WARN("\nICP score is too large, calculate failed.\n");
				Output.get()->clear();
				Output = (Source).get()->makeShared();
				return Mat4f().setZero();
			}

			Eigen::AngleAxisf angle;
			Eigen::Matrix4f Tran = icp.getFinalTransformation();
			angle.fromRotationMatrix(Tran.block<3,3>(0,0));
			std::cout.precision(7);
			double Distance = Tran(0,3)*Tran(0,3) + Tran(1,3)*Tran(1,3) + Tran(2,3)*Tran(2,3);
			std::cout << " Angle is " << angle.angle() * TO_DEG << ". Translation distance is " << Distance << std::endl;

			if(angle.angle() * TO_DEG > Setting.AngleLimit)
			{
				std::cout << "ICP angle is over the limit, discard the transform." << std::endl;
				Output.get()->clear();
				Output = (Source).get()->makeShared();
				return Tran;
			}
			if (Distance > Setting.TranLimit * Setting.TranLimit)
			{
				std::cout << "ICP translation is over the limit, discard the transform." << std::endl;
				Output.get()->clear();
				Output = (Source).get()->makeShared();
				return Tran;
			}
			pcl::transformPointCloud(*Source, *Output, Tran);
			return Tran;
		}
		else
		{
			PCL_ERROR("\nICP has not converged.\n");
			return Mat4f().setZero();
		}
	}
}