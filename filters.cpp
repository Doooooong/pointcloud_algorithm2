#include "filters.hpp"

namespace CookTeam
{
/**
	 * @brief Filters for preprocessing. 
	 * @par Usage: 
	 * 1. Prepare input and output point cloud pointer. 
	 * 2. Make a instance of {@link FilterSetting} struct, and setup parameters you need. 
	 * 3. Using "|" operator to combine the filter specify bits specific filters you want.
	 * 	3.1 Filter specify bits: {@link Filter_FastBilateral}, {@link Filter_Voxel}, {@link Filter_Pass}, {@link Filter_Stat}.
	 * @code
	 * FiltersSetting fSetting;
	 * {
			fSetting.FilterFieldName = "z";
			fSetting.FilterLimitsMin = 0.0f;
			fSetting.FilterLimitsMax = 1.0f;

			fSetting.LeafSizeX = 0.05f;
			fSetting.LeafSizeY = 0.05f;
			fSetting.LeafSizeZ = 0.05f;

			fSetting.MeanK = 50;
			fSetting.StddevMulThresh = 0.6f;

			fSetting.SigmaS = 1;
			fSetting.SigmaR = 0.02;
		}
		// Use all four filters.
		PCXYZ_Ptr ret = Filters(Source, ret2, fSetting, Filter_FastBilateral | Filter_Voxel | Filter_Pass | Filter_Stat);
		// Use filters without FastBilateral filter.
		ret = Filters(Source, ret2, fSetting, Filter_Voxel | Filter_Pass | Filter_Stat);
	* @endcode
	* 
	* @note {@link FilterSetting} has default value.
	* 
	* @param Source Oringin point cloud pointer.
	* @param Output Output point cloud pointer.
	* @param Setting {@link FilterSetting} type setup struct for each filter.
	* @param FilterMask Decided which filter will be used.
	* @return PCXYZ_Ptr Output point cloud pointer.
	*/
PCXYZ_Ptr Filters(PCXYZ_Ptr Source, PCXYZ_Ptr &Output, FiltersSetting Setting, uint8_t FilterMask)
{
	//////// Settings of PassFilter
	pcl::PassThrough<pcl::PointXYZ> PassFilter;
	PassFilter.setFilterFieldName(Setting.FilterFieldName);
	PassFilter.setFilterLimits(Setting.FilterLimitsMin, Setting.FilterLimitsMax); // 0.30

	//////// Settings of VoxelGridFilter
	pcl::VoxelGrid<pcl::PointXYZ> VoxelGrid_sor;
	VoxelGrid_sor.setLeafSize(Setting.LeafSizeX, Setting.LeafSizeY, Setting.LeafSizeZ); //Set the size of VoxelGrid.

	//////// Settings of BilateralFilter
	pcl::FastBilateralFilter<pcl::PointXYZ> fbf;
	fbf.setSigmaS(Setting.SigmaS);
	fbf.setSigmaR(Setting.SigmaR);

	//////// Settings of StatisticalOutlierRemover
	pcl::StatisticalOutlierRemoval<pcl::PointXYZ> sor;
	sor.setMeanK(Setting.MeanK); //50
	sor.setStddevMulThresh(Setting.StddevMulThresh);

	//////// Temporary variables for this function.
	PCXYZ_Ptr tmp(new PCXYZ);
	PCXYZ_Ptr tmp1(new PCXYZ);
	PCXYZ_Ptr tmp2(new PCXYZ);
	//	PCXYZ_Ptr tmp4(new PCXYZ);

	//////// Do the FastBilateralFilter.
	if ((FilterMask & 0x01) == 0x01)
	{
		// CovertTo_OrgnizedPointCloud(Source, 640, 480);
		// fbf.setInputCloud(Source);
		// fbf.filter(*tmp);
		// CovertTo_UnOrgnizedPointCloud(tmp);
	}
	else
		tmp = Source;

	//////// Do the VoxelGrid filter.
	if ((FilterMask & 0x02) == 0x02)
	{
		VoxelGrid_sor.setInputCloud(tmp);
		VoxelGrid_sor.filter(*tmp1);
	}
	else
		tmp1 = tmp;

	//////// Do the Pass filter.
	if ((FilterMask & 0x04) == 0x04)
	{
		PassFilter.setInputCloud(tmp1);
		PassFilter.filter(*tmp2);
	}
	else
		tmp2 = tmp1;

	//////// Do the
	if ((FilterMask & 0x08) == 0x08)
	{
		sor.setInputCloud(tmp2);
		sor.filter(*Output);
	}
	else
		Output = tmp2;
	return Output;
}

PCXYZ_Ptr PassThrough(PCXYZ_Ptr Source, std::string axis, float min, float max)
{
	static PCXYZ_Ptr ret(new PCXYZ);
	static pcl::PassThrough<pcl::PointXYZ> PassFilter;
	PassFilter.setFilterFieldName(axis);
	PassFilter.setInputCloud(Source);
	PassFilter.setFilterLimits(min, max);
	PassFilter.filter(*ret);
	return ret;
}
PCXYZ_Ptr PassThrough(PCXYZ_Ptr Source, std::string axis, Vec3f min, Vec3f max)
{
	static PCXYZ_Ptr ret[3] = {PCXYZ_Ptr(new PCXYZ), PCXYZ_Ptr(new PCXYZ), PCXYZ_Ptr(new PCXYZ)};
	PCXYZ_Ptr input = Source;

	static pcl::PassThrough<pcl::PointXYZ> PassFilter;

	int i = 0;
	for (; i < axis.size() && i < 3; i++)
	{
		if (axis[i] >= 'x' && axis[i] <= 'z')
		{
			PassFilter.setFilterFieldName(std::string(1, axis[i]));
			PassFilter.setInputCloud(input);
			PassFilter.setFilterLimits(min(i), max(i));
			PassFilter.filter(*ret[i]);
		}
		else
			continue;
		input = ret[i];
	}

	return ret[--i];
}

PCXYZ_Ptr VoxelFilter(PCXYZ_Ptr Source, Vec3f voxel_size)
{
	PCXYZ_Ptr ret(new PCXYZ);
	pcl::VoxelGrid<pcl::PointXYZ> VoxelGrid_sor;
	VoxelGrid_sor.setLeafSize(voxel_size(0), voxel_size(1), voxel_size(2)); //Set the size of VoxelGrid.
	VoxelGrid_sor.setInputCloud(Source);
	VoxelGrid_sor.filter(*ret);

	return ret;
}

PCXYZ_Ptr StatisticRemoval(PCXYZ_Ptr Source, uint32_t NumNeighbors, float StdThresh)
{
	PCXYZ_Ptr Output(new PCXYZ);

	pcl::StatisticalOutlierRemoval<pcl::PointXYZ> sor;
	sor.setMeanK(NumNeighbors); //50
	sor.setStddevMulThresh(StdThresh);
	sor.setInputCloud(Source);
	sor.filter(*Output);
	return Output;
}
} // namespace CookTeam