#include "base_functions.hpp"

namespace CookTeam
{
    PCXYZ_Ptr Extract_FromIndices(PCXYZ_Ptr Data, PCIDX_Ptr idx)
    {
        PCXYZ_Ptr res(new PCXYZ);
        for (int index : idx->indices)
            res->points.push_back(Data->points[index]);
        return res;
    }


    void RemovePoints_byModelAABB(PCXYZ_Ptr Source, PCXYZ_Ptr Model, PCXYZ_Ptr Output, double Scale)
    {
        PointXYZ min, max, mid, diff;
        Cal_AABB_MP (Model, min, max);

        mid.x = (max.x + min.x) / 2.0f;
        mid.y = (max.y + min.y) / 2.0f;
        mid.z = (max.z + min.z) / 2.0f;

        diff.x = (max.x - mid.x) * Scale;
        diff.y = (max.y - mid.y) * Scale;
        diff.z = (max.z - mid.z) * Scale;

        Output = PassThrough(Source, "xyz", Vec3f(mid.x - diff.x, mid.y - diff.y, mid.z - diff.z), Vec3f(mid.x + diff.x, mid.y + diff.y, mid.z + diff.z));
    }


    void RemovePoints_byAABB(PCXYZ_Ptr Source, PointXYZ &min, PointXYZ &max, PCXYZ_Ptr Output, double Scale)
    {
        PointXYZ mid, diff;

        mid.x = (max.x + min.x) / 2.0f;
        mid.y = (max.y + min.y) / 2.0f;
        mid.z = (max.z + min.z) / 2.0f;

        diff.x = (max.x - mid.x) * Scale;
        diff.y = (max.y - mid.y) * Scale;
        diff.z = (max.z - mid.z) * Scale;

        Output = PassThrough(Source, "xyz", Vec3f(mid.x - diff.x, mid.y - diff.y, mid.z - diff.z), Vec3f(mid.x + diff.x, mid.y + diff.y, mid.z + diff.z));
    }


    void RemovePoints_byNeartest(PCXYZ_Ptr Source, PCXYZ_Ptr Model, PCXYZ_Ptr OutPut, PCXYZ_Ptr OutPut_Neg, double radius)
    {
        pcl::KdTreeFLANN<PointXYZ> kdtree(false);
        kdtree.setInputCloud(Source);
        pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
        std::vector<float> pointRadiusSquaredDistance;

        for(PointXYZ point : Model->points)
        {
            std::vector<int> pointIdxNKNSearch;
            if ( kdtree.radiusSearch (point, radius, pointIdxNKNSearch, pointRadiusSquaredDistance) > 0 )
                inliers->indices.insert(inliers->indices.end(), pointIdxNKNSearch.begin(), pointIdxNKNSearch.end());
        }
        std::sort(inliers->indices.begin(), inliers->indices.end());
        std::vector<int>::iterator it;
        it = std::unique(inliers->indices.begin(), inliers->indices.end());
        inliers->indices.resize( std::distance(inliers->indices.begin(),it));

        pcl::ExtractIndices<pcl::PointXYZ> extract;
        extract.setInputCloud(Source);
        extract.setIndices(inliers);

        extract.setNegative(false);
        extract.filter(*OutPut_Neg);

        extract.setNegative(true);
        extract.filter(*OutPut);
    }



    std::vector<float> GetNearPoints_Radius(PCXYZ_Ptr Source, PointXYZ Point, PCXYZ_Ptr &OutPut, double radius, bool NeedRest, PCXYZ_Ptr OutPut_Neg)
    {
        pcl::KdTreeFLANN<PointXYZ> kdtree(false);
        kdtree.setInputCloud(Source);
        std::vector<float> pointRadiusSquaredDistance;
        pcl::PointIndices::Ptr indices(new pcl::PointIndices);

        if ( kdtree.radiusSearch (Point, radius, indices->indices, pointRadiusSquaredDistance) <= 0 )
        {
            __DEBUG_PROBE__("GetNearPoints not found and nearest points!");
            OutPut = PCXYZ_Ptr(new PCXYZ);
            OutPut_Neg = PCXYZ_Ptr(new PCXYZ);
            return pointRadiusSquaredDistance;
        }

        pcl::ExtractIndices<pcl::PointXYZ> extract;
        extract.setInputCloud(Source);
        extract.setIndices(indices);
        extract.setNegative(false);
        // Get the points without model.
        extract.filter(*OutPut);
        if (NeedRest)
        {
            // Extract the rest
            extract.setNegative(true);
            extract.filter(*OutPut_Neg);
        }
        return pointRadiusSquaredDistance;
    }


    std::vector<float> GetNearPoints_NearestNum(PCXYZ_Ptr Source, PointXYZ Point, PCXYZ_Ptr &OutPut, int NearestNum, bool NeedRest, PCXYZ_Ptr OutPut_Neg)
    {
        pcl::KdTreeFLANN<PointXYZ> kdtree(false);
        kdtree.setInputCloud(Source);
        std::vector<float> pointRadiusSquaredDistance;
        pcl::PointIndices::Ptr indices(new pcl::PointIndices);

        if ( kdtree.nearestKSearchT(Point, NearestNum, indices->indices, pointRadiusSquaredDistance) <= 0 )
        {
            __DEBUG_PROBE__("GetNearPoints not found and nearest points!");
            OutPut = PCXYZ_Ptr(new PCXYZ);
            OutPut_Neg = PCXYZ_Ptr(new PCXYZ);
            return pointRadiusSquaredDistance;
        }

        pcl::ExtractIndices<pcl::PointXYZ> extract;
        extract.setInputCloud(Source);
        extract.setIndices(indices);
        extract.setNegative(false);
        // Get the points without model.
        extract.filter(*OutPut);
        if (NeedRest)
        {
            // Extract the rest
            extract.setNegative(true);
            extract.filter(*OutPut_Neg);
        }
        return pointRadiusSquaredDistance;
    }

    void GetPoints_Near2PointsLine(PCXYZ_Ptr Source, PointXYZ P1, PointXYZ P2, PCXYZ_Ptr &OutPut, double radius, double delta)
    {
        float Distance = CalDistance(P2, P1);
        int LoopTimes = Distance / delta;
        PointXYZ LineDirectVector = (P2 - P1) / Distance;
        PointXYZ Diff = LineDirectVector * delta;
        PointXYZ Cur(P1), Next(0, 0, 0);
        PCXYZ_Ptr Res(new PCXYZ);
        PCXYZ_Ptr tmp(new PCXYZ);
        PCXYZ_Ptr Model(new PCXYZ);

        // Prepare points of serach center for pickup.
        for(int i = 0; i < LoopTimes; i++)
        {
            Model->points.push_back(Cur);
            Cur = Cur + Diff;
    //		__DEBUG_PROBE__("Search points: ", Cur);
        }
        if(Cur != P2)
            Model->points.push_back(P2);

        // Decent number of points.
        PCXYZ_Ptr Decent(new PCXYZ);
        GetNearPoints_Radius(Source, (P2 + P1) / 2.0f, Decent, Distance / 2 * 1.5f);

        // Pickup points.
        pcl::KdTreeFLANN<PointXYZ> kdtree(false);
        kdtree.setInputCloud(Decent);
        pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
        std::vector<float> pointRadiusSquaredDistance;

        for(PointXYZ point : Model->points)
        {
            std::vector<int> pointIdxNKNSearch;
            if ( kdtree.radiusSearch (point, radius, pointIdxNKNSearch, pointRadiusSquaredDistance) > 0 )
                inliers->indices.insert(inliers->indices.end(), pointIdxNKNSearch.begin(), pointIdxNKNSearch.end());
        }
        std::sort(inliers->indices.begin(), inliers->indices.end());
        std::vector<int>::iterator it;
        it = std::unique(inliers->indices.begin(), inliers->indices.end());
        inliers->indices.resize( std::distance(inliers->indices.begin(),it));

        pcl::ExtractIndices<pcl::PointXYZ> extract;
        extract.setInputCloud(Decent);
        extract.setIndices(inliers);
        extract.setNegative(false);
        // Get the points without model.
        extract.filter(*OutPut);

    //	PCXYZ_Ptr tmp1[3] = {Source, Decent, OutPut};
    //	PCShow(tmp1, "Project test.", 3);
    //	__DEBUG_PROBE__("Found points: ", OutPut->points.size());
    }


    PCXYZ_Ptr TransformPC_viaPoint(PCXYZ_Ptr Data, Vec3f Point, Eigen::Affine3f Rotation)
    {
        PCXYZ_Ptr Transd(new PCXYZ);
        PCXYZ_Ptr Transd2(new PCXYZ);
        Eigen::Affine3f Tran2(Eigen::AngleAxisf(0, Vec3f(1, 0, 0)));
        Rotation.translate(-Point);
        Tran2.translate(Point);
        pcl::transformPointCloud(*Data, *Transd, Rotation.matrix());
        pcl::transformPointCloud(*Transd, *Transd2, Tran2.matrix());

        return Transd2;
    }
    PCXYZ_Ptr TransformPC_viaPoint(PCXYZ_Ptr Data, PointXYZ Point, Eigen::Affine3f Rotation)
    {
        Vec3f PointVec = Cvt_PointXYZ2Vec3f(Point);
        return TransformPC_viaPoint(Data, PointVec, Rotation);
    }

    bool CompairPoint_X_Larger(PointXYZ l, PointXYZ r)
    {
        return l.x > r.x;
    }
    bool CompairPoint_Y_Larger(PointXYZ l, PointXYZ r)
    {
        return l.y > r.y;
    }
    bool CompairPoint_Z_Larger(PointXYZ l, PointXYZ r)
    {
        return l.z > r.z;
    }
    bool CompairPoint_X_Lower(PointXYZ l, PointXYZ r)
    {
        return l.x < r.x;
    }
    bool CompairPoint_Y_Lower(PointXYZ l, PointXYZ r)
    {
        return l.y < r.y;
    }
    bool CompairPoint_Z_Lower(PointXYZ l, PointXYZ r)
    {
        return l.z < r.z;
    }
}